import 'package:dio/dio.dart';

typedef Success = void Function(dynamic jsaon);
typedef Fail = void Function(String? reason, int? code);
typedef After = void Function();

class Http {
  static Dio? _dio;
  static Http https = Http();

  static Http getInstance() {
    //实例化对象
    return https;
  }

  Http() {
    _dio ??= createDio();
  }

  Dio createDio() {
    var dio = Dio(BaseOptions(
      connectTimeout: 10000,
      receiveTimeout: 30000,
      sendTimeout: 30000,
//      baseUrl: 'http://api.tianapi.com',
      responseType: ResponseType.json,
    ));

    return dio;
  }

  Future get(String url, Map<String, dynamic> params,
      {required Success success,
      required Fail fail,
      required After after}) async {
    _dio!.get(url, queryParameters: params).then((response) {
      if (response.statusCode == 200) {
        if (success != null) {
          success(response.data);
        }
      } else {
        if (fail != null) {
          fail(response.statusMessage, response.statusCode);
        }
      }

      if (after != null) {
        after();
      }
    });
  }

  Future post(String url, Map<String, dynamic> params,
      {required Success success,
      var data,
      required Fail fail,
      required After after}) async {
    _dio!.post(
      url,
      data: data,
      queryParameters: params,
    ).then((response) {
      print("response: $response");
      if (response.statusCode == 200) {
        success(response.data);
      } else {
        print('失败');
        fail(response.statusMessage, response.statusCode);
      }
      after();
    });
  }
}
