import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class ProviderWidget<T extends ChangeNotifier> extends StatefulWidget {
  // 用ProviderWidget传入一个泛型
  final Widget Function(BuildContext context, T value, Widget? child) builder;
  final T model;
  final Widget child;
  final Function(T) onReady;

  ProviderWidget({
    required this.model,
    required this.builder,
    required this.child,
    required this.onReady,
  });

  @override
  _ProviderWidgetState createState() => _ProviderWidgetState<T>();
}

class _ProviderWidgetState<T extends ChangeNotifier>
    extends State<ProviderWidget<T>> {
  @override
  void initState() {
    super.initState();
    widget.onReady(widget.model);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
        create: (_) => widget.model,
        child: Consumer<T>(
          //Consumer接收所有的变化
          builder: widget.builder,
          child: widget.child,
        ));
  }
}
