import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:fluttertoast/fluttertoast.dart';


class ApiRequest{
  /// 添加联系人
  Future addContacts(String contactPhone)async{
    Http().post(Urls.ADD_CONTACTS, {},
        data: {
          "phone": contactPhone
        },
        success: (json) {},
        fail: (reason, statusCode) {
          Fluttertoast.showToast(
              msg: reason.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              fontSize: 16.0);
          print(reason);
        }, after: () {});
  }

  showFailReason(){

  }
}