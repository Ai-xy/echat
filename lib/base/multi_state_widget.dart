/// 通用状态页面封装

import 'package:e_chart_mvvm/base/http/base_state.dart';
import 'package:flutter/cupertino.dart';

typedef Content = Widget Function(BuildContext context);

class MultiStateWidget extends StatefulWidget {
  Widget? loadingWidget;
  Widget? emptyWidget;
  Widget? failWidget;
  Content? builder;
  BaseState? state;

//  MultiStateWidget({
//    required this.builder,
//    required this.state,
//    required this.loadingWidget,
//    required this.emptyWidget,
//    required this.failWidget,
//  });
  MultiStateWidget({
    Key? key,
    required Content builder,
    required this.state,
    required this.loadingWidget,
    required this.emptyWidget,
    required this.failWidget,
  }) : super(key: key) {
    if (state == BaseState.CONTENT) {
      this.builder = builder;
    }
    emptyWidget = const EmptyStateWidget();
    failWidget = const FailStateWidget();
    loadingWidget = const LoadingStateWidget();
  }

  @override
  _MultiStateWidgetState createState() => _MultiStateWidgetState();
}

class _MultiStateWidgetState extends State<MultiStateWidget> {
  @override
  Widget build(BuildContext context) {
    print("buildMultiStateWidget");
    if (widget.state == BaseState.CONTENT) {
      return widget.builder!(context);
    } else if (widget.state == BaseState.EMPTY) {
      return widget.emptyWidget!;
    } else if (widget.state == BaseState.FAIL) {
      return widget.failWidget!;
    } else {
      return widget.loadingWidget!;
    }
  }
}

class LoadingStateWidget extends StatelessWidget {
  const LoadingStateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return const Center(
      child: Text("Loading..."),
    );
  }
}

class EmptyStateWidget extends StatelessWidget {
  const EmptyStateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return const Center(
      child: Text("Empty"),
    );
  }
}

class FailStateWidget extends StatelessWidget {
  const FailStateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return const Center(
      child: Text("Fail"),
    );
  }
}
