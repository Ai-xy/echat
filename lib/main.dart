import 'dart:io';
import 'package:e_chart_mvvm/view/splash_page.dart';
import 'package:e_chart_mvvm/viewmodel/theme_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'common/Routes.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  HttpOverrides.global = MyHttpOverrides();
//  runApp(ProviderWidget<SignalRViewModel>(
//      model: SignalRViewModel(),
//      builder: (context, model, child) {
//
//      },
//      child: Container(),
//      onReady: (model) {}));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      builder: () => ChangeNotifierProvider<ThemeViewModel>(
        create: (_) => ThemeViewModel(),
        child: Builder(
          builder: (context) => MaterialApp(
            onGenerateRoute: (setting) {
              return Routes.findRoutes(setting);
            },
            theme: Provider.of<ThemeViewModel>(context, listen: true).themeData,
            home: const SplashPage(),
          ),
        ),
      ),
    );
  }
}
