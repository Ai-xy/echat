import 'package:sqflite/sqflite.dart';

final String tableName = 'table_user';
final String userId = 'user_id';
final String userName = 'user_name';
final String userPwd = 'user_pwd';

class RegisterUser {
  int? id;
  String? name;
  String? pwd;
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      userName: name,
      userPwd: pwd,
    };
    if (id != null) {
      map[userId] = id;
    }
    return map;
  }

  // 生成构造函数
  RegisterUser();
  RegisterUser.fromMap(Map<String, dynamic> map) {
    id = map[userId];
    name = map[userName];
    pwd = map[userPwd];
  }
}

class TableReUserProvider {
  Database? db;
  Future open(String path) async {
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
          create table $tableName (
          $userId integer primary key autoincrement,
          $userName text not null,
          $userPwd text not null)
          ''');
    });
  }

  // 向表里插入id字段
  Future<RegisterUser> insert(RegisterUser registerUser) async {
    registerUser.id = await db!.insert(tableName, registerUser.toMap());
    return registerUser;
  }

  // 查询表里对应id的数据
  Future<RegisterUser> getTodo(int id, String path, Database db) async {
    List<Map<String, dynamic>> maps = await db.query(tableName,
        columns: [userId, userName, userPwd],
        where: '$userId=?',
        whereArgs: [id]);
    if (maps.isNotEmpty) {
      return RegisterUser.fromMap(maps.first);
    }
    return RegisterUser();
  }

  // 查询表里对应id的数据
  Future<RegisterUser> getTodo2(int id) async {
    List<Map<String, dynamic>> maps = await db!.query(tableName,
        columns: [userId, userName, userPwd],
        where: '$userId=?',
        whereArgs: [id]);
    if (maps.isNotEmpty) {
      return RegisterUser.fromMap(maps.first);
    }
    return RegisterUser();
  }

  // 删除表里的某条记录
  Future<int> delete(int id) async {
    return await db!.delete(tableName, where: '$userId=?', whereArgs: [id]);
  }

  // 修改表里的某条记录
  Future<int> update(RegisterUser registerUser, String path, Database db,
      String tableName) async {
    return await db.update(tableName, registerUser.toMap(),
        where: '$userId=?', whereArgs: [registerUser.id]);
  }

  // 关闭数据库
  Future close(Database db) async => db.close();
  // 关闭数据库
  Future close2() async => db!.close();
}
