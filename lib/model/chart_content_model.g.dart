// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chart_content_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatCountModel _$ChatCountModelFromJson(Map<String, dynamic> json) =>
    ChatCountModel(
      user: (json['user'] as List<dynamic>?)
          ?.map((e) => User.fromJson(e as Map<String, dynamic>))
          .toList(),
      isRead: json['isRead'] as bool?,
      lastContent: json['lastContent'] as String?,
      lastUpdateTime: json['lastUpdateTime'] as String?,
    );

Map<String, dynamic> _$ChatCountModelToJson(ChatCountModel instance) =>
    <String, dynamic>{
      'user': instance.user,
      'lastContent': instance.lastContent,
      'lastUpdateTime': instance.lastUpdateTime,
      'isRead': instance.isRead,
    };
