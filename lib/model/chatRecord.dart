// ignore_for_file: file_names
import 'package:json_annotation/json_annotation.dart';
part 'chatRecord.g.dart';

@JsonSerializable()
class ChatRecord{
  int? sender;
  String? content;
  String? avatarUrl;
  ChatRecord({this.sender,this.content,this.avatarUrl});

  factory ChatRecord.fromJson(Map<String, dynamic> json) =>
      _$ChatRecordFromJson(json);
  Map<String, dynamic> toJson() => _$ChatRecordToJson(this);
}
