/// Mianyang Qingmu Software Technology Co., Ltd. <br>
/// Copyright (C) 2014-2022 All Rights Reserved. <br>
/// Website: http://www.qmrjkj.com <br>
/// Author: 540631855@qq.com <br>
/// Date: 2022-01-24 <br>
/// Time: 21:40 <br>
/// Description:

import 'package:json_annotation/json_annotation.dart';
part 'response_results.g.dart';

@JsonSerializable()
class ResponseResults {
  int? code;
  String? message;
  ResponseResults({this.code, this.message});

  factory ResponseResults.fromJson(Map<String, dynamic> json) =>
      _$ResponseResultsFromJson(json);
  Map<String, dynamic> toJson() => _$ResponseResultsToJson(this);
}
