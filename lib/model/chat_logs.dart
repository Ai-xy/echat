/// Author: 540631855@qq.com <br>
/// Date: 2022-02-15 <br>
/// Time: 11:17 <br>
/// Description:

import 'package:json_annotation/json_annotation.dart';
import 'chatRecord.dart';
part 'chat_logs.g.dart';

@JsonSerializable()
class ChatLogs {
  List<ChatRecord>? logs;
  ChatLogs({this.logs});

  factory ChatLogs.fromJson(Map<String, dynamic> json) =>
      _$ChatLogsFromJson(json);
  Map<String, dynamic> toJson() => _$ChatLogsToJson(this);
}