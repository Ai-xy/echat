// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_results.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseResults _$ResponseResultsFromJson(Map<String, dynamic> json) =>
    ResponseResults(
      code: json['code'] as int?,
      message: json['message'] as String?,
    );

Map<String, dynamic> _$ResponseResultsToJson(ResponseResults instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
    };
