// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      id: json['id'] as int?,
      nickName: json['nickName'] as String?,
      sex: json['sex'] as String?,
      sign: json['sign'] as String?,
      location: json['location'] as String?,
      selectedPhotos: json['selectedPhotos'] as String?,
      age: json['age'] as String?,
      birthday: json['birthday'] as String?,
      avatarUrl: json['avatarUrl'] as String?,
      password: json['password'] as String?,
      state: json['state'] as int?,
      phone: json['phone'] as String?,
      msgCode: json['msgCode'] as String?,
      regTime: json['regTime'] as String?,
      connID: json['connID'] as String?,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'phone': instance.phone,
      'password': instance.password,
      'msgCode': instance.msgCode,
      'regTime': instance.regTime,
      'nickName': instance.nickName,
      'state': instance.state,
      'sex': instance.sex,
      'sign': instance.sign,
      'location': instance.location,
      'birthday': instance.birthday,
      'avatarUrl': instance.avatarUrl,
      'selectedPhotos': instance.selectedPhotos,
      'age': instance.age,
      'connID': instance.connID,
    };
