// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chatRecord.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatRecord _$ChatRecordFromJson(Map<String, dynamic> json) => ChatRecord(
      sender: json['sender'] as int?,
      content: json['content'] as String?,
      avatarUrl: json['avatarUrl'] as String?,
    );

Map<String, dynamic> _$ChatRecordToJson(ChatRecord instance) =>
    <String, dynamic>{
      'sender': instance.sender,
      'content': instance.content,
      'avatarUrl': instance.avatarUrl,
    };
