// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_test.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserTest _$UserTestFromJson(Map<String, dynamic> json) => UserTest(
      nickName: json['nickName'] as String?,
      password: json['password'] as String?,
      state: json['state'] as String?,
      phone: json['phone'] as String?,
    );

Map<String, dynamic> _$UserTestToJson(UserTest instance) => <String, dynamic>{
      'nickName': instance.nickName,
      'password': instance.password,
      'state': instance.state,
      'phone': instance.phone,
    };
