// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'send_message_template_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SendMessageTemplateModel _$SendMessageTemplateModelFromJson(
        Map<String, dynamic> json) =>
    SendMessageTemplateModel(
      fromWho: json['fromWho'] == null
          ? null
          : User.fromJson(json['fromWho'] as Map<String, dynamic>),
      toWho: json['toWho'] == null
          ? null
          : User.fromJson(json['toWho'] as Map<String, dynamic>),
      message: json['message'] as String?,
      sendTime: json['sendTime'] as String?,
    );

Map<String, dynamic> _$SendMessageTemplateModelToJson(
        SendMessageTemplateModel instance) =>
    <String, dynamic>{
      'fromWho': instance.fromWho,
      'toWho': instance.toWho,
      'message': instance.message,
      'sendTime': instance.sendTime,
    };
