// ignore_for_file: file_names

import 'package:sqflite/sqflite.dart';

final String tableName = 'table_chatList';
final String chatList = 'chat_name';
final String userId = 'user_id';
final String contactPhone = 'contact_phone';

class ChatList {
  int? id;
  String? mChatList;
  String? mContactPhone;
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      chatList: mChatList,
      contactPhone: mContactPhone
    };
    if (id != null) {
      map[userId] = id;
    }
    return map;
  }

  // 生成构造函数
  ChatList();
  ChatList.fromMap(Map<String, dynamic> map) {
    id = map[userId];
    mChatList = map[chatList];
    mContactPhone = map[contactPhone];
  }
}

class TableChatListProvider {
  Database? db;

  Future open(String path, Database? database) async {
    if (database != null) {
      database = await openDatabase(path, version: 1);
      print("数据库不为空！");
    } else {
      print("数据库空！");
      database = await openDatabase(path, version: 1,
          onCreate: (Database db, int version) async {
        await db.execute('''
          create table $tableName (
          $userId integer primary key autoincrement,
          $chatList text not null,
          $contactPhone text not null)
          ''');
      });
    }
    return database;
  }

  Future open2(String path) async {
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
          create table $tableName (
          $userId integer primary key autoincrement,
          $chatList text not null,
          $contactPhone text not null)
          ''');
    });
  }

  // 向表里插入id字段
  Future<ChatList> insert(ChatList chatList) async {
    chatList.id = await db!.insert(tableName, chatList.toMap());
    return chatList;
  }

//  // 查询表里对应id的数据
//  Future<ChatList> getTodo(
//      int id, String path, String contactPhone, Database db) async {
//    List<Map<String, dynamic>> maps = await db.query(tableName,
//        columns: [userId, chatList], where: '$userId=?', whereArgs: [id]);
//    if (maps.isNotEmpty) {
//      return ChatList.fromMap(maps.first);
//    }
//    return ChatList();
//  }
//
//  // 查询表里对应id的数据
//  Future<ChatList> getTodo2(int id) async {
//    List<Map<String, dynamic>> maps = await db!.query(tableName,
//        columns: [userId, chatList], where: '$userId=?', whereArgs: [id]);
//    if (maps.isNotEmpty) {
//      return ChatList.fromMap(maps.first);
//    }
//    return ChatList();
//  }

  // 根据联系人电话查询表里对应id的数据
  Future<ChatList> getDataUseContactPhone(String phone, Database db) async {
    List<Map<String, dynamic>> maps = await db.query(tableName,
        columns: [contactPhone, chatList],
        where: '$contactPhone=?',
        whereArgs: [phone]);
    if (maps.isNotEmpty) {
      return ChatList.fromMap(maps.first);
    }
    return ChatList();
  }

  // 根据联系人电话查询表里对应id的数据
  Future<ChatList> getDataUseContactPhone2(String phone) async {
    List<Map<String, dynamic>> maps = await db!.query(tableName,
        columns: [contactPhone, chatList],
        where: '$contactPhone=?',
        whereArgs: [phone]);
    if (maps.isNotEmpty) {
      print(maps);
      return ChatList.fromMap(maps.first);
    }
    return ChatList();
  }

  // 删除表里的某条记录
  Future<int> delete(int id) async {
    return await db!.delete(tableName, where: '$userId=?', whereArgs: [id]);
  }

  // 修改表里的某条记录
  Future<int> update(ChatList chatList, String phone) async {
    return await db!.update(tableName, chatList.toMap(),
        where: '$contactPhone=?', whereArgs: [phone]);
  }

  // 关闭数据库
  Future close(Database db) async => db.close();
  // 关闭数据库
  Future close2() async => db!.close();
}
