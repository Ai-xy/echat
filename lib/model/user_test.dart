import 'package:json_annotation/json_annotation.dart';
part 'user_test.g.dart';

@JsonSerializable()
class UserTest {
  String? nickName;
  String? password;
  String? state;
  String? phone;
  UserTest(
      {
        this.nickName,
        this.password,
        this.state,
        this.phone});

  factory UserTest.fromJson(Map<String, dynamic> json) => _$UserTestFromJson(json);
  Map<String, dynamic> toJson() => _$UserTestToJson(this);
}
