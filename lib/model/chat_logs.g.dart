// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_logs.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatLogs _$ChatLogsFromJson(Map<String, dynamic> json) => ChatLogs(
      logs: (json['logs'] as List<dynamic>?)
          ?.map((e) => ChatRecord.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ChatLogsToJson(ChatLogs instance) => <String, dynamic>{
      'logs': instance.logs,
    };
