class FriendCircleBean {
  int? code;
  String? msg;
  List<Newslist>? newslist;

  FriendCircleBean(
      {required this.code, required this.msg, required this.newslist});

  FriendCircleBean.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    msg = json['msg'];
    if (json['newslist'] != null) {
      newslist = <Newslist>[];
      json['newslist'].forEach((v) {
        newslist!.add(Newslist.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['code'] = code;
    data['msg'] = msg;
    if (newslist != null) {
      data['newslist'] = newslist!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Newslist {
  String? content;
  String? source;

  Newslist({required this.content, required this.source});

  Newslist.fromJson(Map<String, dynamic> json) {
    content = json['content'];
    source = json['source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['content'] = this.content;
    data['source'] = this.source;
    return data;
  }
}
