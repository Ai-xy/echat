import 'package:json_annotation/json_annotation.dart';
part 'add_contacts_results.g.dart';

@JsonSerializable()
class AddContactsResults {
  List<String>? contactsList;
  AddContactsResults({this.contactsList});

  factory AddContactsResults.fromJson(Map<String, dynamic> json) =>
      _$AddContactsResultsFromJson(json);
  Map<String, dynamic> toJson() => _$AddContactsResultsToJson(this);
}
