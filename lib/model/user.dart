import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';

@JsonSerializable()
class User {
  int? id;
  String? phone;
  String? password;
  String? msgCode;
  String? regTime;
  String? nickName;
  int? state;
  String? sex;
  String? sign;
  String? location;
  String? birthday;
  String? avatarUrl;
  String? selectedPhotos;
  String? age;
  String? connID;

  User(
      {this.id,
      this.nickName,
      this.sex,
      this.sign,
      this.location,
      this.selectedPhotos,
      this.age,
      this.birthday,
      this.avatarUrl,
      this.password,
      this.state,
      this.phone,
      this.msgCode,
      this.regTime,
      this.connID});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
