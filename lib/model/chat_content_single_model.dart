import 'package:e_chart_mvvm/model/user.dart';
import 'package:json_annotation/json_annotation.dart';
part 'chat_content_single_model.g.dart';

@JsonSerializable()
class ChatContentSingleModel {
  User? user;
  String? lastContent;
  String? lastUpdateTime;
  bool? isRead;

  ChatContentSingleModel(
      {this.user, this.isRead, this.lastContent, this.lastUpdateTime});
  factory ChatContentSingleModel.fromJson(Map<String, dynamic> json) =>
      _$ChatContentSingleModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChatContentSingleModelToJson(this);
}