import 'package:e_chart_mvvm/model/user.dart';
import 'package:json_annotation/json_annotation.dart';
part 'chart_content_model.g.dart';

@JsonSerializable()
class ChatCountModel {
  List<User>? user;
  String? lastContent;
  String? lastUpdateTime;
  bool? isRead;

  ChatCountModel(
      {this.user, this.isRead, this.lastContent, this.lastUpdateTime});
  factory ChatCountModel.fromJson(Map<String, dynamic> json) =>
      _$ChatCountModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChatCountModelToJson(this);
}
