// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_content_single_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatContentSingleModel _$ChatContentSingleModelFromJson(
        Map<String, dynamic> json) =>
    ChatContentSingleModel(
      user: json['user'] == null
          ? null
          : User.fromJson(json['user'] as Map<String, dynamic>),
      isRead: json['isRead'] as bool?,
      lastContent: json['lastContent'] as String?,
      lastUpdateTime: json['lastUpdateTime'] as String?,
    );

Map<String, dynamic> _$ChatContentSingleModelToJson(
        ChatContentSingleModel instance) =>
    <String, dynamic>{
      'user': instance.user,
      'lastContent': instance.lastContent,
      'lastUpdateTime': instance.lastUpdateTime,
      'isRead': instance.isRead,
    };
