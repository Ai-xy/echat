// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_contacts_results.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddContactsResults _$AddContactsResultsFromJson(Map<String, dynamic> json) =>
    AddContactsResults(
      contactsList: (json['contactsList'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$AddContactsResultsToJson(AddContactsResults instance) =>
    <String, dynamic>{
      'contactsList': instance.contactsList,
    };
