import 'package:e_chart_mvvm/model/user.dart';
import 'package:json_annotation/json_annotation.dart';
part 'send_message_template_model.g.dart';

@JsonSerializable()

class SendMessageTemplateModel{
  User? fromWho;
  User? toWho;
  String? message;
  String? sendTime;
  SendMessageTemplateModel({this.fromWho,this.toWho,this.message,this.sendTime});


  factory SendMessageTemplateModel.fromJson(Map<String, dynamic> json) =>
      _$SendMessageTemplateModelFromJson(json);
  Map<String, dynamic> toJson() => _$SendMessageTemplateModelToJson(this);
}