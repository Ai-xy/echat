/// 注册页面

import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/common/Routes.dart';
import 'package:e_chart_mvvm/viewmodel/register_view_model.dart';
import 'package:e_chart_mvvm/widget/appbar/appbar_transparent.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return ProviderWidget<RegisterViewModel>(
      model: RegisterViewModel(),
      onReady: (model) {
        model.init();
      },
      builder: (context, model, child) {
        return buildBody(context, model);
      },
      child: Container(),
    );
  }
}

/// 主干
buildBody(BuildContext context, RegisterViewModel model) {
  return Scaffold(
    appBar: _buildAppBar(context),
    body: SingleChildScrollView(
      child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: [
                    newUserRegister(context),
                    nameBox(context, model),
                    Container(
                      margin: EdgeInsets.only(left: 16.w, right: 16.w),
                      child: TextField(
                        controller: model.pwdController,
                        maxLength: 16,
                        maxLines: 1,
                        obscureText: model.pswVisible,
                        decoration: InputDecoration(
                          counter: const Text(''),
                          hintText: "请输入登录密码",
                          hintStyle:
                              TextStyle(fontSize: 16.sp, color: Colors.grey),
                          suffixIcon: IconButton(
                            icon: model.pswVisible
                                ? const Icon(Icons.visibility_off)
                                : const Icon(Icons.visibility),
                            onPressed: () {
                              model.changeEyeState();
                            },
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 16.w, right: 16.w, bottom: 16.h),
                      child: TextField(
                        controller: model.pwdControllerAgain,
                        maxLength: 16,
                        maxLines: 1,
                        obscureText: model.pswVisible,
                        decoration: InputDecoration(
                          counter: const Text(''),
                          hintText: "请再次输入登录密码",
                          hintStyle:
                              TextStyle(fontSize: 16.sp, color: Colors.grey),
                          suffixIcon: IconButton(
                            icon: model.pswVisible
                                ? const Icon(Icons.visibility_off)
                                : const Icon(Icons.visibility),
                            onPressed: () {
                              model.changeEyeState();
                            },
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                _loginButton(context,model),
                registerButton(context, model)
              ],
            ),
          )),
    ),
  );
}

/// 标题
_buildAppBar(BuildContext context) {
  return TransparentAppBar(
          leading: InkWell(
            onTap: (){
              Navigator.of(context).pop();
            },
            child: Padding(
              padding: EdgeInsets.all(18.w),
              child: SizedBox(
                height: 30.w,
                width: 30.w,
                child: const Image(image: AssetImage('img/arrow_left.png')),
              ),
            ),
          ),
          titleName: '')
      .build(context);
}

/// '用手机号注册'标语
Widget newUserRegister(BuildContext context) {
  return Container(
    alignment: Alignment.topLeft,
    padding: EdgeInsets.only(top: 16.h, left: 16.w, right: 16.w),
    child: Text(
      '用手机号注册',
      style: TextStyle(fontSize: 40.sp),
    ),
  );
}

/// 已有账号点击登录按钮
Widget _loginButton(BuildContext context, RegisterViewModel model) {
  return GestureDetector(
    onTap: () {
      Navigator.of(context).pushNamed(Routes.login_page);
    },
    child: Text(
      '已有账号，点击登录',
      style: TextStyle(
        color: Colors.blue,
        decoration: TextDecoration.underline,
        fontSize: 14.0.sp,
      ),
    ),
  );
}

/// 账号框
Widget nameBox(BuildContext context, RegisterViewModel model) {
  return Container(
    margin: EdgeInsets.only(top: 16.h, left: 16.w, right: 16.w),
    child: TextField(
      controller: model.phoneController,
      maxLength: 15,
      maxLines: 1,
      decoration: InputDecoration(
        hintText: "输入手机号",
        hintStyle: TextStyle(fontSize: 16.sp, color: Colors.grey),
        counter: Text(''),
      ),
    ),
  );
}

/// 注册按钮
Widget registerButton(BuildContext context, RegisterViewModel model) {
  return GestureDetector(
    onTap: () {
      model.register(context);
    },
    child: Container(
        margin: EdgeInsets.only(top: 36.h),
        decoration: BoxDecoration(
          boxShadow: const [
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, 15.0), //阴影xy轴偏移量
                blurRadius: 15.0, //阴影模糊程度
                spreadRadius: 1.0 //阴影扩散程度
            )
          ],
          color: Colors.blue,
          borderRadius: BorderRadius.all(Radius.circular(30.w)),
        ),
        child: Padding(
          padding: EdgeInsets.all(16.w),
          child: SizedBox(
            width: 300.w,
            child: Center(
              child: Text(
                "下一步",
                style: TextStyle(color: Colors.white, fontSize: 16.sp),
              ),
            ),
          ),
        )),
  );
}
