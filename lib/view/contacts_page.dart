import 'package:badges/badges.dart';
import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/common/Routes.dart';
import 'package:e_chart_mvvm/view/user_info_page.dart';
import 'package:e_chart_mvvm/viewmodel/contacts_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:signalr_client/hub_connection.dart';

class ContactsPage extends StatefulWidget {
  final HubConnection? connection;
  const ContactsPage({Key? key, this.connection}) : super(key: key);

  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: ProviderWidget<ContactsViewModel>(
        model: ContactsViewModel(),
        onReady: (model) {
          model.init(widget.connection!);
        },
        builder: (context, model, child) {
          return ListView.builder(
              itemCount: model.contactsList.length + 2,
              itemBuilder: (BuildContext context, int i) {
                if (i == 0) {
                  return _buildAddNewContactsWidget(context, model);
                } else if (i == 1) {
                  return _buildNewContactsWidget(
                      context, model, widget.connection!);
                } else {
                  return _buildContactsWidget(
                      context, model, i, widget.connection!);
                }
              });
        },
        child: Container(),
      ),
    );
  }
}

/// 添加联系人
_buildAddNewContactsWidget(BuildContext context, ContactsViewModel model) {
  return InkWell(
      onTap: () {
        model.alertDialog(context);
      },
      child: Container(
        padding: EdgeInsets.all(6.w),
        child: ListTile(
          leading: Container(
            height: 56.w,
            width: 56.w,
            decoration: ShapeDecoration(
                image: const DecorationImage(
                  image: AssetImage('img/add_person.png'),
                  fit: BoxFit.cover,
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(40))),
          ),
          title: Text(
            '新建联系人',
            style: TextStyle(
              color:
                  Theme.of(context).bottomNavigationBarTheme.selectedItemColor,
            ),
          ),
        ),
      ));
}

/// 新的好友申请
_buildNewContactsWidget(
    BuildContext context, ContactsViewModel model, HubConnection connection) {
  return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed(Routes.new_contacts_page).then((value) {
          if (value == 'ref') {
            model.init(connection);
          }
        });
      },
      child: Container(
        padding: EdgeInsets.all(6.w),
        child: ListTile(
          leading: model.newApplyLength == 0
              ? Container(
                  height: 56.w,
                  width: 56.w,
                  decoration: ShapeDecoration(
                      image: const DecorationImage(
                        image: AssetImage('img/new_person.png'),
                        fit: BoxFit.cover,
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadiusDirectional.circular(40))),
                )
              : Badge(
                  badgeContent: Text(
                    model.newApplyLength.toString(),
                    style: const TextStyle(color: Colors.white),
                  ),
                  child: Container(
                    height: 56.w,
                    width: 56.w,
                    decoration: ShapeDecoration(
                        image: const DecorationImage(
                          image: AssetImage('img/new_person.png'),
                          fit: BoxFit.cover,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadiusDirectional.circular(40))),
                  ),
                ),
          title: Text(
            '新朋友',
            style: TextStyle(
              color:
                  Theme.of(context).bottomNavigationBarTheme.selectedItemColor,
            ),
          ),
        ),
      ));
}

/// 联系人
_buildContactsWidget(BuildContext context, ContactsViewModel model, int i,
    HubConnection connection) {
  return InkWell(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => UserInfoPage(
                    user: model.contactsList[i - 2],
                    connection: connection,
                  )));
    },
    child: Container(
      padding: EdgeInsets.all(6.w),
      child: ListTile(
        leading: Container(
          height: 56.w,
          width: 56.w,
          decoration: ShapeDecoration(
              image: const DecorationImage(
                image: AssetImage('img/atnw.jpg'),
                fit: BoxFit.cover,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(40))),
        ),
        title: Text(model.contactsList[i - 2].nickName!),
      ),
    ),
  );
}
