import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/model/user.dart';
import 'package:e_chart_mvvm/viewmodel/user_info_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:signalr_client/hub_connection.dart';

class UserInfoPage extends StatefulWidget {
  final User? user;
  final HubConnection? connection;
  const UserInfoPage({Key? key, this.user, this.connection}) : super(key: key);

  @override
  _UserInfoPageState createState() => _UserInfoPageState();
}

class _UserInfoPageState extends State<UserInfoPage>
    with TickerProviderStateMixin {
  AnimationController? animationController;
  Animation<double>? anim;
  User? user;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    anim = Tween(begin: 0.0, end: 0.0).animate(animationController!);
  }

  @override
  void dispose() {
    super.dispose();
    animationController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ProviderWidget<UserInfoViewModel>(
        model: UserInfoViewModel(),
        onReady: (model) {
          if (widget.user == null) {
            user = model.userInfo;
          } else {
            user = widget.user;
          }
          model.init(animationController, anim, user!, widget.connection);
        },
        builder: (context, model, child) {
          return Scaffold(
              backgroundColor: Theme.of(context).backgroundColor,
              body: Listener(
                  onPointerMove: (result) {
                    model.updatePicHeight(result.position.dy);
                  },
                  onPointerDown: (result) {
                    model.init_dy = result.position.dy;
                  },
                  onPointerUp: (_) {
                    model.runAnimate();
                    model.animationController!.forward(from: 0);
                  },
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Flexible(
                          child: Stack(
                            children: [
                              _buildUserInfoBody(context, model),
                              _buildAppBar(context, model)
                            ],
                          ),
                        ),
                        _buildSendMessageButton(context, model)
                      ])));
        },
        child: Container());
  }
}

/// 渐变标题栏
_buildAppBar(BuildContext context, UserInfoViewModel model) {
  return Positioned(
    top: 0,
    child: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        height: 56 + MediaQuery.of(context).padding.top,
        width: MediaQuery.of(context).size.width,
        color: Color.fromARGB(model.trans, 30, 155, 255),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              child: IconButton(
                icon: const Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.w),
              child: Text(
                '我的资料',
                style: TextStyle(
                    color: Color.fromARGB(model.trans, 255, 255, 255),
                    fontSize: 16.sp),
              ),
            ),
            InkWell(
              child: IconButton(
                icon: const Icon(
                  Icons.menu_outlined,
                  color: Colors.white,
                ),
                onPressed: () {},
              ),
            ),
          ],
        )),
  );
}

/// 个人资料身体构建
_buildUserInfoBody(BuildContext context, UserInfoViewModel model) {
  return CustomScrollView(
    controller: model.controller,
    slivers: <Widget>[
      SliverToBoxAdapter(
          child: Container(
        color: Theme.of(context).backgroundColor,
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(
                  //缩放的图片
                  width: MediaQuery.of(context).size.width,
                  child: Image.asset("img/user_bg.jpg",
                      height: 200.h + model.extraPicHeight, fit: BoxFit.cover),
                ),
                SizedBox(
                  height: 60.h,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 30.w, top: 10.h),
                        child: Text("phone：" + model.userInfo.phone.toString()),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              left: 30.w,
              top: 150.h + model.extraPicHeight,
              child: Container(
                height: 100.w,
                width: 100.w,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 2),
                    image: const DecorationImage(
                      image: AssetImage('img/head_portrait.png'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(50.w))),
              ),
            ),
            Positioned(
                left: 140.w,
                top: 160.h + model.extraPicHeight,
                child: Text(model.userInfo.nickName!,
                    style: TextStyle(
                        fontSize: 26.sp,
                        color: Colors.white,
                        fontWeight: FontWeight.w500))),
          ],
        ),
      )),
      _buildInfoList(context, model)
    ],
  );
}

/// 个人信息列表构建
_buildInfoList(BuildContext context, UserInfoViewModel model) {
  return SliverToBoxAdapter(
      child: Column(
    children: <Widget>[
      SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildUserLocationAndSignItem('img/head.png',
                model.userInfo.sex! + ": " + model.userInfo.location!, model),
            _buildUserLocationAndSignItem(
                'img/pen.png', model.userInfo.sign!, model),
            _buildSelectedPhotosItem(context, model),
            _buildPersonalLabel(context, model),
            _buildPersonalLabel(context, model)
          ],
        ),
      ),
    ],
  ));
}

/// 构造个人地址签名项
_buildUserLocationAndSignItem(
    String leading, String info, UserInfoViewModel model) {
  return InkWell(
    onTap: () {

    },
    child: ListTile(
      minLeadingWidth: 16.w,
      leading: Padding(
        padding: EdgeInsets.only(top: 3.h),
        child: SizedBox(
          height: 18.w,
          width: 18.w,
          child: Image(
            image: AssetImage(leading),
          ),
        ),
      ),
      title: Text(
        info,
        style: TextStyle(fontSize: 14.sp),
      ),
      trailing: SizedBox(
        height: 16.w,
        width: 16.w,
        child: const Image(
          image: AssetImage('img/arrow_right.png'),
        ),
      ),
    ),
  );
}

/// 精选照片
_buildSelectedPhotosItem(BuildContext context, UserInfoViewModel model) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      InkWell(
        onTap: () {},
        child: ListTile(
          title: Text(
            "精选照片",
            style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
          ),
          trailing: SizedBox(
            height: 16.w,
            width: 16.w,
            child: const Image(
              image: AssetImage('img/arrow_right.png'),
            ),
          ),
        ),
      ),
      SizedBox(
        height: 200.h,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.only(left: 16.w, right: 16.w, bottom: 16.h),
            child: Image(
              image: AssetImage(model.userInfo.selectedPhotos!),
              fit: BoxFit.cover,
            )),
      )
    ],
  );
}

/// 个性标签
_buildPersonalLabel(BuildContext context, UserInfoViewModel model) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      InkWell(
        onTap: () {},
        child: ListTile(
          title: Text(
            "个性标签",
            style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w600),
          ),
          trailing: SizedBox(
            height: 16.w,
            width: 16.w,
            child: const Image(
              image: AssetImage('img/arrow_right.png'),
            ),
          ),
        ),
      ),
      SizedBox(
        height: 200.h,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.only(left: 16.w, right: 16.w, bottom: 16.h),
            child: Center(
              child: Text('Mpower',
                  style:
                      TextStyle(fontSize: 36.sp, fontWeight: FontWeight.w600)),
            )),
      )
    ],
  );
}

/// 发送消息按钮
_buildSendMessageButton(BuildContext context, UserInfoViewModel model) {
  return GestureDetector(
    onTap: () {
      model.toChatPage(context);
    },
    child: Container(
      color: Theme.of(context).cardColor,
      child: SizedBox(
        height: 60.h,
        child: Center(
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 10.h),
              child: Text('发消息',
                  style: TextStyle(
                      color: Theme.of(context)
                          .bottomNavigationBarTheme
                          .selectedItemColor))),
        ),
      ),
    ),
  );
}
