import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/model/send_message_template_model.dart';
import 'package:e_chart_mvvm/model/user.dart';
import 'package:e_chart_mvvm/viewmodel/chart_viewmodel.dart';
import 'package:signalr_client/hub_connection.dart';
import '../widget/appbar/echart_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:bubble/bubble.dart';

class ChartPage extends StatefulWidget {
  final User user;
  final HubConnection? connection;
  final List<SendMessageTemplateModel>? message;
  const ChartPage({Key? key, required this.user, this.connection, this.message})
      : super(key: key);

  @override
  _ChartPageState createState() => _ChartPageState();
}

class _ChartPageState extends State<ChartPage> {

  int len = 0;
  @override
  Widget build(BuildContext context) {
    return ProviderWidget<ChartViewModel>(
        model: ChartViewModel(widget.connection!, widget.user),
        onReady: (model) {
          if (widget.message != null) {
            len = widget.message!.length;
            model.init(
                context, widget.user, widget.connection!, widget.message!);
          } else {
            model.init(context, widget.user, widget.connection!, []);
          }
        },
        builder: (context, model, child) {
          if (widget.message != null) {
            if (len != widget.message!.length) {
              model.getData(widget.message!);
            }
          }

          return Scaffold(
              backgroundColor:
                  Theme.of(context).bottomNavigationBarTheme.backgroundColor,
              appBar: _buildAppBar(context, widget.user.nickName!),
              body: Stack(
                children: [
                  Column(
                    children: [
                      buildMessageList(model),
                      _buildTextInputBox(context, model)
                    ],
                  ),
                  Positioned(
                    bottom: 50.h,
                    right: 16.w,
                    child: Offstage(
                      offstage: model.ifNewMsg,
                      child: InkWell(
                        onTap: () {
                          model.moveToButton();
                        },
                        child: Container(
                            decoration: ShapeDecoration(
                                color: Colors.blueAccent,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadiusDirectional.circular(
                                            10.w))),
                            child: const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                "新信息",
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                      ),
                    ),
                  )
                ],
              ));

//          if (model.provider!.connId == null) {
//            return const Scaffold(
//              body: Center(child: CircularProgressIndicator()),
//            );
//          } else {
//            return Scaffold(
//                backgroundColor:
//                    Theme.of(context).bottomNavigationBarTheme.backgroundColor,
//                appBar: _buildAppBar(context),
//                body: Column(
//                  children: [
//                    buildMessageList(model),
//                    _buildTextInputBox(context, model)
//                  ],
//                ));
//          }
        },
        child: Container());
  }
}

/// 构造appbar
_buildAppBar(BuildContext context, String user) {
  return EChartAppBar(title: user, canBack: true, actions: [
    InkWell(
      onTap: () {
        //model.addChatListToSq(model.messageList.toString(),context);
      },
      child: Padding(
          padding: EdgeInsets.only(right: 16.w),
          child: const Icon(Icons.menu_outlined)),
    )
  ]).build(context);
}

/// 构建消息列表
Widget buildMessageList(ChartViewModel model) {
  return Flexible(
      child: ListView.builder(
          controller: model.msgListController,
          itemCount: model.messageList!.length,
          itemBuilder: (BuildContext context, int index) {
            if (model.messageList![index].sender == 0) {
              return buildMessageWidget(context, model, index);
            } else {
              return buildSendMessageWidget(context, model, index);
            }
          }));
}

/// 构建接收消息子项
Widget buildMessageWidget(
    BuildContext context, ChartViewModel model, int index) {
  return Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: EdgeInsets.all(16.w),
        child: Container(
          height: 50.w,
          width: 50.w,
          decoration: ShapeDecoration(
              image: DecorationImage(
                image: AssetImage(model.messageList![index].avatarUrl!),
                fit: BoxFit.cover,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(40.w))),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 16.w),
        child: Bubble(
          color: Theme.of(context).cardColor,
          nip: BubbleNip.leftTop,
          child: Container(
            constraints: BoxConstraints(
              maxWidth: 200.w,
            ),
            child: Text(
              model.messageList![index].content!,
              style: TextStyle(fontSize: 16.sp),
            ),
            padding: EdgeInsets.all(6.w),
          ),
        ),
      )
    ],
  );
}

/// 构建回复消息子项
Widget buildSendMessageWidget(
    BuildContext context, ChartViewModel model, int index) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: EdgeInsets.only(top: 16.w),
        child: Bubble(
          color: Theme.of(context).primaryColor,
          nip: BubbleNip.rightTop,
          child: Container(
            constraints: BoxConstraints(
              maxWidth: 200.w,
            ),
            child: Text(
              model.messageList![index].content!,
              style: TextStyle(fontSize: 16.sp, color: Colors.white),
            ),
            padding: EdgeInsets.all(6.w),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.all(16.w),
        child: Container(
          height: 50.w,
          width: 50.w,
          decoration: ShapeDecoration(
              image: DecorationImage(
                image: AssetImage(model.messageList![index].avatarUrl!),
                fit: BoxFit.cover,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(40.w))),
        ),
      )
    ],
  );
}

/// 构造输入框
Widget _buildTextInputBox(BuildContext context, ChartViewModel model) {
  return Column(
    children: [
      Container(
          height: 38.h,
          padding: EdgeInsets.symmetric(horizontal: 12.w),
          child: Row(children: <Widget>[
            Flexible(
                child: Container(
              padding: EdgeInsets.all(10.w),
              decoration: ShapeDecoration(
                  color: Theme.of(context).cardColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadiusDirectional.circular(30.w))),
              child: TextField(
                controller: model.sendController,
                //onSubmitted: _handleSubmitted,
                decoration: InputDecoration.collapsed(
                    hintText: '发送消息', hintStyle: TextStyle(fontSize: 14.sp)),
              ),
            )),
            InkWell(
              onTap: () {
                model.sendMessage(context);
              },
              child: Container(
                margin: EdgeInsets.only(left: 6.w),
                width: 70.w,
                decoration: ShapeDecoration(
                    color: model.sendButtonColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadiusDirectional.circular(40.w))),
                child: const Center(
                    child: Text(
                  '发送',
                  style: TextStyle(color: Colors.white),
                )),
              ),
            )
          ])),
    ],
  );
}
