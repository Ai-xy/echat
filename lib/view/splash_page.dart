/// 开屏页

import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/viewmodel/splash_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    return ProviderWidget<SplashViewModel>(
      model: SplashViewModel(),
      onReady: (model) {
        model.init(context);
      },
      builder: (context, model, child) {
        return Scaffold(
            backgroundColor: Colors.white, body: buildSplashWidget());
      },
      child: Container(),
    );
  }
}

buildSplashWidget() {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(),
        Column(
          children: [
            SizedBox(
              height: 120.h,
              width: 120.h,
              child: const Padding(
                padding: EdgeInsets.all(6),
                child: Image(image: AssetImage('img/e_chart.png')),
              ),
            ),
            Text('e-聊', style: TextStyle(fontSize: 26.sp)),
          ],
        ),
        Text(
          '加密通话，妈妈再也不用担心我被监听了！',
          style: TextStyle(color: Colors.grey, fontSize: 16.sp),
        )
      ],
    ),
  );
}
