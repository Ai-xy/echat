import 'dart:io';
import 'package:badges/badges.dart';
import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/model/chat_content_single_model.dart';
import 'package:e_chart_mvvm/model/send_message_template_model.dart';
import 'package:e_chart_mvvm/view/chart_page.dart';
import 'package:e_chart_mvvm/viewmodel/message_viewmodel.dart';
import 'package:e_chart_mvvm/widget/slide_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:signalr_client/hub_connection.dart';

class MessagePage extends StatefulWidget {
  final HubConnection? connection;
  //final ChatContentSingleModel? message;
  final List<SendMessageTemplateModel>? messageList;
  const MessagePage({Key? key, this.connection, this.messageList})
      : super(key: key);

  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {
  int len = 0;
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print('message销毁');
  }

  @override
  Widget build(BuildContext context) {
    return ProviderWidget<MessageViewModel>(
      model: MessageViewModel(),
      onReady: (model) {
        len = widget.messageList!.length;
        model.init(widget.connection, widget.messageList!);
      },
      builder: (context, model, child) {
        for (var item in widget.messageList!) {
          print('获得数据：' +
              item.fromWho!.phone! +
              ' ' +
              item.message! +
              ' ' +
              item.sendTime!.toString());
        }
        if (len != widget.messageList!.length) {
          print("refresh");
          model.refresh(widget.messageList!);
          len = widget.messageList!.length;
        }
        return model.buildPage(context, model);
      },
      child: Container(),
    );
  }
}

///构建消息列表
Widget buildNewsList(MessageViewModel model, BuildContext context) {
  return Container(
    color: Theme.of(context).backgroundColor,
    child: ScrollConfiguration(
        // 苹果和安卓不一样
        behavior: MyBehavior(),
        child: Listener(
          onPointerMove: (e) {
            if (model.isMove) {
              model.slideKey.currentState.close();
            }
          },
          child: ListView.builder(
              controller: model.msgListController,
              itemCount: model.chats.length,
              itemBuilder: (BuildContext context, int index) {
                if (model.chats.isNotEmpty) {
                  var key = GlobalKey<SlideButtonState>();
                  return BuildItem(
                    mKey: key,
                    index: index,
                    model: model,
                    child: buildMessageItem(context, model, index),
                  );
                }
                return Container();
              }),
        )),
  );
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    if (Platform.isAndroid || Platform.isFuchsia) {
      return child;
    } else {
      return super.buildViewportChrome(context, child, axisDirection);
    }
  }
}

/// 构建消息列表子项
// ignore: must_be_immutable
class BuildItem extends StatelessWidget {
  var mKey;
  MessageViewModel model;
  Widget child;
  int index;
  BuildItem(
      {Key? key,
      this.mKey,
      required this.child,
      required this.index,
      required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SlideButton(
      key: mKey,
      singleButtonWidth: 80.w,
      child: Container(
        height: 70.h,
        color: Theme.of(context).backgroundColor,
        child: child,
      ),
      buttons: <Widget>[
        buildAction("标记未读", Colors.orange, () {
          model.removeItem(index);
          mKey.currentState.close();
        }),
        buildAction("删除", Colors.redAccent, () {
          model.removeItem(index);
          mKey.currentState.close();
        }),
      ],
      onSlideCanceled: () {},
      onSlideStarted: () {
        model.slideKey = mKey;
      },
      onSlideCompleted: () {
        model.slideKey = mKey;
        model.isMove = true;
      },
    );
  }
}

/// 消息子项
buildMessageItem(BuildContext context, MessageViewModel model, int index) {
  return InkWell(
    onTap: () {
      model.unreadMessagesNum[index] = 0;
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChartPage(
                    user: model.chats[index].user!,
                    connection: model.mConnection,
                    message: model.testList[index],
                  )));
    },
    child: Container(
        padding: EdgeInsets.only(left: 16.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 50.w,
                  width: 50.w,
                  decoration: ShapeDecoration(
                      image: const DecorationImage(
                        image: AssetImage(
//                            model.chats[index].userId![0].avatarUrl!),
                            'img/head_portrait.png'),
                        fit: BoxFit.cover,
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadiusDirectional.circular(40.w))),
                ),
                Container(
                  padding: EdgeInsets.only(left: 16.w, top: 16.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
//                        child: Text(model.chats[index].user![0].nickName!,
                        child: Text(model.chats[index].user!.nickName!,
                            style: TextStyle(fontSize: 18.sp)),
                      ),
                      Container(
                        child: Text(
                          model.chats[index].lastContent!,
                          style: TextStyle(color: Colors.grey, fontSize: 14.sp),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(right: 16.w),
              child: Badge(
                badgeContent: Text(
                  model.testList[index].length.toString(),
                  style: const TextStyle(color: Colors.white),
                ),
                child: Container(
                  height: 15.h,
                  padding: EdgeInsets.only(right: 16.w),
                  child: Text(
                    model.chats[index].lastUpdateTime!,
                    style: TextStyle(color: Colors.grey, fontSize: 12.sp),
                  ),
                ),
              ),
            ),
          ],
        )),
  );
}

/// 构建删除、置顶按钮
InkWell buildAction(String text, Color color, GestureTapCallback tap) {
  return InkWell(
    onTap: tap,
    child: Container(
      alignment: Alignment.center,
      width: 80.w,
      color: color,
      child: Text(text,
          style: const TextStyle(
            color: Colors.white,
          )),
    ),
  );
}

/// 无消息时的页面
Widget nullPage(BuildContext context) {
  return Container(
    color: Theme.of(context).backgroundColor,
    child: const Center(
      child: Text('暂无消息'),
    ),
  );
}
