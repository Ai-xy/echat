import 'dart:ui';

import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/viewmodel/login_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
  Animation<double>? animation;
  AnimationController? controller;
  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 200), vsync: this);
    animation = Tween(begin: 200.0, end: 70.0).animate(controller!);
    animation!.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("img/bg_login.jpg"), fit: BoxFit.cover),
        ),
        child: Container(
          alignment: Alignment.center,
          color: Colors.grey.withOpacity(0.1),
          child: SingleChildScrollView(
            child: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: ProviderWidget<LoginViewModel>(
                  model: LoginViewModel(),
                  onReady: (model) {
                    model.init();
                  },
                  builder: (context, model, child) {
                    return Column(
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
//                            _buildTextBar(context),
                            _logoBox(context),
                            _inPutWidget(context, model),
                            _buildBoxSelected(model),
                            _registerButton(context, model)
                          ],
                        ),
                        _loginButton(context, model)
                      ],
                    );
                  },
                  child: Container(),
                )),
          ),
        ),
//        child: ClipRRect(
//          child: BackdropFilter(
//            //背景滤镜
//            filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5), //背景模糊化
//
//          ),
//        ),
      ),
    );
  }

  /// 提示
  _buildTextBar(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding:
          EdgeInsets.only(top: 16.h, left: 16.w, right: 16.w, bottom: 36.h),
      child: Text(
        '用手机号登录',
        style: TextStyle(fontSize: 36.sp, color: Colors.white),
      ),
    );
  }

  /// logo框
  Widget _logoBox(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 16.h, bottom: 16.h),
          height: 120.w,
          width: 120.w,
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("img/e_chart.png"), fit: BoxFit.cover),
          ),
        ),
        Text('e-聊天',
            style: TextStyle(
                color: Theme.of(context).textTheme.subtitle2!.color,
                fontSize: 20.sp,
                fontWeight: FontWeight.w500))
      ],
    );
  }

  _inPutWidget(BuildContext context, LoginViewModel model) {
    return Container(
      margin: EdgeInsets.all(16.w),
      decoration: BoxDecoration(
        boxShadow: const [
          BoxShadow(
              color: Colors.black26,
              offset: Offset(0.0, 15.0), //阴影xy轴偏移量
              blurRadius: 15.0, //阴影模糊程度
              spreadRadius: 1.0 //阴影扩散程度
              )
        ],
        color: const Color.fromARGB(255, 255, 255, 255),
        borderRadius: BorderRadius.all(Radius.circular(6.w)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _nameBox(context, model),
          Divider(height: 1.h, color: const Color(0xffd8e3e7)),
          _pwdBox(context, model),
        ],
      ),
    );
  }

  /// 账号框
  Widget _nameBox(BuildContext context, LoginViewModel model) {
    return Container(
      padding:
          EdgeInsets.only(top: 6.0.h, left: 16.0.w, right: 16.0.w, bottom: 6.h),
      child: TextField(
        controller: model.phoneController,
        maxLines: 1,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "请输入手机号",
          hintStyle: TextStyle(fontSize: 14.sp, color: Colors.grey),
        ),
      ),
    );
  }

  /// 密码框
  Widget _pwdBox(BuildContext context, LoginViewModel model) {
    return Container(
      padding:
          EdgeInsets.only(top: 6.h, left: 16.0.w, right: 16.0.w, bottom: 6.h),
      child: TextField(
        controller: model.pwdController,
        maxLines: 1,
        obscureText: model.pswVisible,
        decoration: InputDecoration(
          hintText: "请输入登录密码",
          border: InputBorder.none,
          hintStyle: TextStyle(fontSize: 14.sp, color: Colors.grey),
          suffixIcon: IconButton(
            icon: model.pswVisible
                ? const Icon(Icons.visibility_off)
                : const Icon(Icons.visibility),
            onPressed: () {
              model.changeEyeState();
            },
          ),
        ),
      ),
    );
  }

  /// 记住密码选择框
  Widget _buildBoxSelected(LoginViewModel model) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Checkbox(
            value: model.checkboxSelected,
            onChanged: (value) {
              model.changeBoxSelectedState();
            }),
        Padding(
          padding: EdgeInsets.only(right: 20.w),
          child: const Text('记住密码', style: TextStyle(color: Colors.white)),
        )
      ],
    );
  }

  /// 注册按钮
  Widget _registerButton(BuildContext context, LoginViewModel model) {
    return GestureDetector(
      onTap: () {
        model.toRegisterPage(context);
      },
      child: Text(
        '没有账号？点击注册',
        style: TextStyle(
          color: Colors.blue,
          decoration: TextDecoration.underline,
          fontSize: 14.0.sp,
        ),
      ),
    );
  }

  /// 登录按钮
  Widget _loginButton(BuildContext context, LoginViewModel model) {
    return GestureDetector(
        onTap: () {
          model.login(context,controller!);
        },
        child: animation!.value > 75.0
            ? Container(
                width: animation!.value.w,
                height: 40.h,
                margin: EdgeInsets.only(top: 36.h),
                decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(15.0, 15.0),
                        blurRadius: 15.0,
                        spreadRadius: 1.0)
                  ],
                  color: const Color(0xff1a94bc),
                  borderRadius: BorderRadius.all(Radius.circular(30.w)),
                ),
                child: SizedBox(
                  child: Center(
                    child: Text(
                      "登录",
                      style: TextStyle(color: Colors.white, fontSize: 16.sp),
                    ),
                  ),
                ))
            : Container(
                height: 40.h,
                margin: EdgeInsets.only(top: 36.h),
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(30)),
                child: const CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                )));
  }
}
