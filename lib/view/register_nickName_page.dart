// ignore_for_file: file_names

import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/viewmodel/register_nickName_viewModel.dart';
import 'package:e_chart_mvvm/widget/appbar/appbar_transparent.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RegisterNickNamePage extends StatefulWidget {
  final String userPhone;
  const RegisterNickNamePage({Key? key,required this.userPhone}) : super(key: key);

  @override
  _RegisterNickNamePageState createState() => _RegisterNickNamePageState();
}

class _RegisterNickNamePageState extends State<RegisterNickNamePage> {
  @override
  Widget build(BuildContext context) {
    return ProviderWidget<RegisterNickNameViewModel>(
      model: RegisterNickNameViewModel(),
      onReady: (model) {
        model.init(widget.userPhone);
      },
      builder: (context, model, child) {
        return buildBody(context, model);
      },
      child: Container(),
    );
  }
}

/// 主干
buildBody(BuildContext context, RegisterNickNameViewModel model) {
  return Scaffold(
    appBar: _buildAppBar(context),
    body: SingleChildScrollView(
      child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: [
                    newUserRegister(context),
                    nickNameBox(context, model),
                  ],
                ),
                registerButton(context, model)
              ],
            ),
          )),
    ),
  );
}

/// 标题
_buildAppBar(BuildContext context) {
  return TransparentAppBar(
          leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Padding(
              padding: EdgeInsets.all(18.w),
              child: SizedBox(
                height: 30.w,
                width: 30.w,
                child: const Image(image: AssetImage('img/arrow_left.png')),
              ),
            ),
          ),
          titleName: '')
      .build(context);
}

/// '请输入昵称'标语
Widget newUserRegister(BuildContext context) {
  return Container(
    alignment: Alignment.topLeft,
    padding: EdgeInsets.only(top: 16.h, left: 16.w, right: 16.w),
    child: Text(
      '请输入昵称',
      style: TextStyle(fontSize: 40.sp),
    ),
  );
}

/// 输入昵称框
Widget nickNameBox(BuildContext context, RegisterNickNameViewModel model) {
  return Container(
    margin: EdgeInsets.only(top: 16.h, left: 16.w, right: 16.w),
    child: TextField(
      controller: model.nickNameController,
      maxLength: 15,
      maxLines: 1,
      decoration: InputDecoration(
        hintText: "请输入昵称",
        hintStyle: TextStyle(fontSize: 16.sp, color: Colors.grey),
        counter: const Text(''),
      ),
    ),
  );
}

/// 注册按钮
Widget registerButton(BuildContext context, RegisterNickNameViewModel model) {
  return GestureDetector(
    onTap: () {
      model.changNickName(context);
    },
    child: Container(
        margin: EdgeInsets.only(top: 36.h),
        decoration: BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.all(Radius.circular(30.w)),
        ),
        child: Padding(
          padding: EdgeInsets.all(16.w),
          child: SizedBox(
            width: 300.w,
            child: Center(
              child: Text(
                "点击注册",
                style: TextStyle(color: Colors.white, fontSize: 16.sp),
              ),
            ),
          ),
        )),
  );
}
