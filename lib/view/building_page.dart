import 'package:flutter/material.dart';

/// Author: 540631855@qq.com <br>
/// Date: 2022-02-18 <br>
/// Time: 11:34 <br>
/// Description:

class BuildingPage extends StatelessWidget {
  const BuildingPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: const Center(
        child: Text('施工队建设中~'),
      ),
    );
  }
}
