/// 接收好友申请页面
import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/viewmodel/new_contacts_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NewContactsPage extends StatefulWidget {
  const NewContactsPage({Key? key}) : super(key: key);

  @override
  _NewContactsPageState createState() => _NewContactsPageState();
}

class _NewContactsPageState extends State<NewContactsPage> {
  @override
  Widget build(BuildContext context) {
    return ProviderWidget<NewContactsViewModel>(
        model: NewContactsViewModel(),
        onReady: (model) {
          model.init();
        },
        builder: (context, model, child) {
          return WillPopScope(
              child: Scaffold(
                backgroundColor: Theme.of(context).backgroundColor,
                appBar: AppBar(
                  title: const Text('新朋友'),
                ),
                body: model.newContactsList.isEmpty
                    ? const Center(child: Text("暂无好友申请~"))
                    : ListView.builder(
                        itemCount: model.newContactsList.length,
                        itemBuilder: (context, index) {
                          return _buildContactsWidget(context, model, index);
                        }),
              ),
              onWillPop: () async {
                Navigator.of(context).pop('ref');
                return true;
              });
        },
        child: Container());
  }
}

/// 联系人
_buildContactsWidget(BuildContext context, NewContactsViewModel model, int i) {
  return InkWell(
    onLongPress: () {
      model.alertDialog(context);
    },
    child: Container(
      padding: EdgeInsets.all(6.w),
      child: ListTile(
        leading: Container(
          height: 56.w,
          width: 56.w,
          decoration: ShapeDecoration(
              image: const DecorationImage(
                image: AssetImage('img/atnw.jpg'),
                fit: BoxFit.cover,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(40))),
        ),
        title: Text(model.newContactsList[i].nickName!),
        trailing: InkWell(
          onTap: () {
            model.passTheContactsApply(i);
          },
          child: Container(
              padding: EdgeInsets.only(
                  top: 6.h, bottom: 6.h, left: 16.w, right: 16.w),
              color: Colors.greenAccent,
              child: const Text(
                '同意',
                style: TextStyle(color: Colors.white),
              )),
        ),
      ),
    ),
  );
}
