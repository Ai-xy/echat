import 'package:e_chart_mvvm/base/provider_widget.dart';
import 'package:e_chart_mvvm/common/Routes.dart';
import 'package:e_chart_mvvm/viewmodel/main_viewmodel.dart';
import 'package:e_chart_mvvm/viewmodel/theme_viewmodel.dart';
import '../widget/appbar/appbarr_headInfo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class MainPage extends StatefulWidget {
  final String phone;
  const MainPage({Key? key, required this.phone}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  void initState() {
    super.initState();
//    Http.getInstance().get('http://www.baidu.com', {}, success: (dynamic json) {
//      print(jsonEncode(json).toString());
//    }, fail: (reason, code) {
//      print(reason);
//    }, after: () {});
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
    return ProviderWidget<MainViewModel>(
      model: MainViewModel(),
      onReady: (model) {
        model.init(widget.phone);
      },
      builder: (context, model, child) {
        model.buildPage(context, model);
        return Scaffold(
          appBar: _buildAppBar(context, model),
          body: model.pages.isEmpty
              ? Container()
              : model.pages[model.currentIndex],
          bottomNavigationBar: BottomNavigationBar(
            selectedItemColor:
                Theme.of(context).bottomNavigationBarTheme.selectedItemColor,
            unselectedItemColor:
                Theme.of(context).bottomNavigationBarTheme.unselectedItemColor,
            backgroundColor:
                Theme.of(context).bottomNavigationBarTheme.backgroundColor,
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.message), label: '消息'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.account_circle), label: '联系人'),
              BottomNavigationBarItem(
                icon: Icon(Icons.ballot),
                label: '动态',
              ),
            ],
            currentIndex: model.currentIndex,
            onTap: (int i) {
              setState(() {
                model.currentIndex = i;
              });
            },
          ),
        );
      },
      child: Container(),
    );

//    ChangeNotifierProvider<MainViewModel>(
//        create: (_) => MainViewModel(),
//        child: Builder(
//            builder: (context) => MaterialApp(
//              home: Scaffold(
//                backgroundColor: Theme.of(context).backgroundColor,
//                appBar: _buildAppBar2(context),
//                body: Container(),
//                bottomNavigationBar: BottomNavigationBar(
//                  selectedItemColor: Theme.of(context)
//                      .bottomNavigationBarTheme
//                      .selectedItemColor,
//                  unselectedItemColor: Theme.of(context)
//                      .bottomNavigationBarTheme
//                      .unselectedItemColor,
//                  backgroundColor: Theme.of(context)
//                      .bottomNavigationBarTheme
//                      .backgroundColor,
//                  items: const [
//                    BottomNavigationBarItem(
//                        icon: Icon(Icons.message), label: '消息'),
//                    BottomNavigationBarItem(
//                        icon: Icon(Icons.account_circle), label: '联系人'),
//                    BottomNavigationBarItem(
//                      icon: Icon(Icons.ballot),
//                      label: '动态',
//                    ),
//                  ],
//                  currentIndex: 0,
//                  onTap: (int i) {
//                    setState(() {
//                      //model.currentIndex = i;
//                    });
//                  },
//                ),
//              ),
//            )));

//          body: Center(
//            child: ProviderWidget<FriendCircleViewModel>(
//              model: FriendCircleViewModel(),
//              onReady: (model) {
//                model.load();
//              },
//              builder: (context, model, child) {
//                return MultiStateWidget(
//                    state: model.state!,
//                    loadingWidget: Container(),
//                    emptyWidget: Container(),
//                    failWidget: Container(),
//                    builder: (context) => _buildInfoWidget());
//              },
//              child: Container(),
//            ),
//          )
  }

  _buildAppBar(BuildContext context, MainViewModel model) {
    return HeadInfoAppBar(
        leading: Padding(
          padding: EdgeInsets.all(10.w),
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(Routes.user_info_page);
            },
            child: Container(
              height: 60.w,
              width: 60.w,
              decoration: ShapeDecoration(
                  image: const DecorationImage(
                    image: AssetImage('img/atnw.jpg'),
                    fit: BoxFit.cover,
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadiusDirectional.circular(40))),
            ),
          ),
        ),
        nickName: model.userInfo.nickName!.isEmpty
            ? '用户001'
            : model.userInfo.nickName!,
        canBack: false,
        actions: [
          InkWell(
            onTap: () {
              Provider.of<ThemeViewModel>(context, listen: false).changeTheme();
            },
            child: Padding(
              padding: EdgeInsets.only(right: 16.w),
              child: const Icon(Icons.lightbulb),
            ),
          )
        ]).build(context);
  }
}
