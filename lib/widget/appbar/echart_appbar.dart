import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EChartAppBar {
  final String title;
  final bool canBack;
  final List<Widget> actions;
  EChartAppBar(
      {required this.title, this.canBack = true, required this.actions});
  PreferredSizeWidget build(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
      centerTitle: true,
      title: Text(title,
          style:
              TextStyle(color: Theme.of(context).appBarTheme.titleTextStyle!.color)),
      automaticallyImplyLeading: canBack,
      actions: actions,
    );
  }
}
