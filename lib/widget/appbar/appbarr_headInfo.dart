// ignore_for_file: file_names

///透明标题栏

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HeadInfoAppBar {
  final String nickName;
  final bool canBack;
  final List<Widget> actions;
  final Widget leading;
  HeadInfoAppBar(
      {required this.leading,
      required this.nickName,
      this.canBack = true,
      required this.actions});
  PreferredSizeWidget build(BuildContext context) {
    return AppBar(
      leading: leading,
      backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
      centerTitle: false,
      title: Text(nickName,
          style: TextStyle(
              color: Theme.of(context).appBarTheme.titleTextStyle!.color)),
      automaticallyImplyLeading: canBack,
      actions: actions,
    );
  }
}
