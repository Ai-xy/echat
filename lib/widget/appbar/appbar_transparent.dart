///透明标题栏

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TransparentAppBar {
  final String? titleName;
  final bool? canBack;
  final List<Widget>? actions;
  final Widget? leading;
  TransparentAppBar(
      {this.leading, this.titleName, this.canBack = true, this.actions});
  PreferredSizeWidget build(BuildContext context) {
    return AppBar(
      leading: leading,
      backgroundColor: const Color.fromARGB(0, 255, 255, 255),
      centerTitle: false,
      title: Text(titleName!,
          style: TextStyle(
              color: Theme.of(context).textTheme.subtitle1!.color,fontSize: 26.sp)),
      automaticallyImplyLeading: canBack!,
      actions: actions,
    );
  }
}
