import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 顶部图片下拉缩放控件

class SliverTopBar extends StatelessWidget {
  final double extraPicHeight;
  final BoxFit fitType;
  const SliverTopBar(
      {Key? key, required this.extraPicHeight, required this.fitType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            SizedBox(
              //缩放的图片
              width: MediaQuery.of(context).size.width,
              child: Image.asset("img/atnw.jpg",
                  height: 180.h + extraPicHeight, fit: fitType),
            ),
            Container(
              height: 80.h,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 16.w, top: 10.h),
                    child: const Text("QQ：54063222"),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 16.w, top: 8.h),
                    child: const Text("男：四川 成都"),
                  )
                ],
              ),
            ),
          ],
        ),
        Positioned(
          left: 30.w,
          top: 130.h + extraPicHeight,
          child: SizedBox(
            width: 100.w,
            height: 100.w,
            child: const CircleAvatar(
              backgroundImage: AssetImage('img/head_portrait.png'),
            ),
          ),
        )
      ],
    );
  }
}
