// ignore_for_file: file_names

import 'package:e_chart_mvvm/view/chart_page.dart';
import 'package:e_chart_mvvm/view/contacts_page.dart';
import 'package:e_chart_mvvm/view/login_page.dart';
import 'package:e_chart_mvvm/view/main_page.dart';
import 'package:e_chart_mvvm/view/new_contacts_page.dart';
import 'package:e_chart_mvvm/view/register_nickName_page.dart';
import 'package:e_chart_mvvm/view/register_page.dart';
import 'package:e_chart_mvvm/view/user_info_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Routes {
  static const String main_page = "/main/mainPage";
  static const String chart_page = "/main/chartPage";
  static const String user_info_page = "/main/userInfoPage";
  static const String register_page = "/main/registerPage";
  static const String login_page = "/main/loginPage";
  static const String register_nickName_page = "/main/registerNickNamePage";
  static const String contacts_page = "/main/contactsPage";
  static const String new_contacts_page = "/main/newContactsPage";


  static Route findRoutes(RouteSettings setting) {
    String? name = setting.name;
    Object? argument = setting.arguments;

    return MaterialPageRoute(builder: (_) {
      return _findPage(name, argument);
    });
  }

  static Widget _findPage(String? name, Object? object) {
    Widget page = const LoginPage();
    switch (name) {
      case main_page:
        //page = const MainPage();
        break;
      case chart_page:
        //page = const ChartPage();
        break;
      case user_info_page:
        page = const UserInfoPage();
        break;
      case register_page:
        page = const RegisterPage();
        break;
      case register_nickName_page:
        //page = const RegisterNickNamePage();
        break;
      case login_page:
        page = const LoginPage();
        break;
      case contacts_page:
        page = const ContactsPage();
        break;
      case new_contacts_page:
        page = const NewContactsPage();
        break;

    }
    return page;
  }
}
