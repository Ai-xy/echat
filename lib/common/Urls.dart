// ignore_for_file: file_names, constant_identifier_names

import 'package:e_chart_mvvm/base/appkey.dart';

class Urls {
  static const String friendsCircle =
      'http://api.tianapi.com/pyqwenan/index?key=' + AppKey.appKey;
  static const String USER_REGISTER =
      'http://169.254.10.233:5000/api/User/AddUser';
  static const String USER_ALTER_NICKNAME =
      'http://169.254.10.233:5000/api/User/AlterNickName';
  static const String USER_ALTER_LOCATION =
      'http://169.254.10.233:5000/api/User/AlterLocation';
  static const String LOGIN =
      'http://169.254.10.233:5000/api/User/Login';
  static const String GET_USER_INFO =
      'http://169.254.10.233:5000/api/User/GetUserInfo';
  static const String ADD_CONTACTS =
      'http://169.254.10.233:5000/api/User/AddContacts';
  static const String GET_ADD_CONTACT =
      'http://169.254.10.233:5000/api/User/GetAddContacts';
  static const String CONFIRM_Add_CONTACTS =
      'http://169.254.10.233:5000/api/User/ConfirmAddContacts';
  static const String BUILD_CONNECTION =
      'http://169.254.10.233:5000/api/User/BuildConnection';
  static const String ADD_CHAT_CONTENT =
      'http://169.254.10.233:5000/api/User/AddChatContent';



}
