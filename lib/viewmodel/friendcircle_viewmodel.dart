import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/base_state.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:e_chart_mvvm/model/friendcircle_bean.dart';
import 'package:flutter/foundation.dart';

class FriendCircleViewModel extends BaseViewModel {
  FriendCircleBean? friendCircleBean;

  load() {
    print('Load');
    state = BaseState.LOADING;
    notifyListeners();
    Http().get(Urls.friendsCircle, {}, success: (json) {
      /// 用子线程解析json，防止ui线程被阻塞
//      compute(decode, json).then((value) {
//        friendCircleBean = value;
//        state = BaseState.CONTENT;
//        notifyListeners();
//      });
      friendCircleBean = FriendCircleBean.fromJson(json);
      state = BaseState.CONTENT;
      notifyListeners();
      print(friendCircleBean!.newslist![0].content);
    }, fail: (reason, statuscode) {
      state = BaseState.FAIL;
      notifyListeners();
      print(reason);
    }, after: () {});
  }

  static FriendCircleBean decode(dynamic json) {
    return FriendCircleBean.fromJson(json);
  }
}
