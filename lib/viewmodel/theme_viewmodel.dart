/// 暗黑模式
import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:flutter/material.dart';

class ThemeViewModel extends BaseViewModel {
  ThemeData themeData = light();

  bool lighted = true;

  changeTheme() {
    if (lighted) {
      themeData = dark();
    } else {
      themeData = light();
    }
    lighted = !lighted;
    notifyListeners();
  }

  static ThemeData light() {
    return ThemeData(
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        primaryColor: Colors.blue,
        cardColor: const Color(0xfff5f5f5),
        textTheme: const TextTheme(
            subtitle1: TextStyle(color: Colors.black),
            subtitle2: TextStyle(color: Colors.white)),
        bottomNavigationBarTheme: const BottomNavigationBarThemeData(
          selectedItemColor: Colors.blueAccent,
          unselectedItemColor: Color(0xff8a8a8a),
          backgroundColor: Color(0xffF5F5F5),
        ),
        appBarTheme: const AppBarTheme(
            color: Colors.blue,
            elevation: 0,
            titleTextStyle: TextStyle(color: Colors.white, fontSize: 16)));
  }

  static ThemeData dark() {
    return ThemeData(
        backgroundColor: Colors.black,
        brightness: Brightness.dark,
        primaryColor: const Color(0xff212121),
        cardColor: const Color(0xff212121),
        textTheme: const TextTheme(
            subtitle1: TextStyle(color: Colors.white),
            subtitle2: TextStyle(color: Colors.black)),
        bottomNavigationBarTheme: const BottomNavigationBarThemeData(
          selectedItemColor: Color(0xff155eb7),
          unselectedItemColor: Color(0xff8a8a8a),
          backgroundColor: Colors.black,
        ),
        appBarTheme: const AppBarTheme(
          color: Colors.black,
          elevation: 0,
          titleTextStyle: TextStyle(color: Colors.white, fontSize: 16),
        ));
  }
}
