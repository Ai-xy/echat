// ignore_for_file: file_names

import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Routes.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:e_chart_mvvm/model/response_results.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RegisterNickNameViewModel extends BaseViewModel {
  var nickNameController = TextEditingController();
  String? userPhone;

  init(String? phone){
    userPhone = phone;
  }

  /// 修改昵称
  changNickName(BuildContext context) {
    if(nickNameController.text.isEmpty){
      Fluttertoast.showToast(
          msg: '请输入昵称',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
    }
    else{
      if(nickNameController.text.length>10){
        Fluttertoast.showToast(
            msg: '昵称长度需小于10',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            fontSize: 16.0);
      }else{
        sendToServer(context);
      }
    }
  }

  /// 提交到服务器
  sendToServer(BuildContext context) {
    ResponseResults responseResults;
    Http().post(Urls.USER_ALTER_NICKNAME, {}, data: {
      "phone": userPhone,
      "nickName": nickNameController.text,
      "state": 1
    }, success: (json) {
      responseResults = ResponseResults.fromJson(json);
      Fluttertoast.showToast(
          msg: responseResults.message.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      if (responseResults.code == 1) {
        print(responseResults.message);
        Navigator.of(context).pushNamed(Routes.login_page);
      }
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {});
  }

}
