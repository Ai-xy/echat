import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:e_chart_mvvm/model/response_results.dart';
import 'package:e_chart_mvvm/model/user.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signalr_client/hub_connection.dart';

class ContactsViewModel extends BaseViewModel {
  HubConnection? connection;
  int? newApplyLength = 0;
  String? userName;
  List<User> contactsList = [];
  TextEditingController contactsController = TextEditingController();

  init(HubConnection connection) {
    this.connection = connection;
    newApplyLength = 0;
    contactsList.clear();
    contactsController.clear();
    getContacts();
  }

  /// 获取用户名
  getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userName = prefs.getString('user_name');
    this.userName = userName;
    print(this.userName);
  }

  /// 获取好友列表
  getContacts() async {
    await getUserName();
    User user;
    print('获取好友列表');
    List<dynamic> resList;
    Http().post(Urls.GET_ADD_CONTACT, {}, data: {
      "contactsPhone": userName,
      "state":2
    }, success: (json) {
      print(json);
      resList = json;
      for (int i = 0; i < resList.length; i++) {
        user = User.fromJson(resList[i]);
        contactsList.add(user);
        notifyListeners();
      }
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {
      getAddContactsApply();
    });
  }

  /// 获取好友申请列表
  getAddContactsApply() async {
    print('获取好友申请列表');
    List<dynamic> resList;
    Http().post(Urls.GET_ADD_CONTACT, {}, data: {
      "contactsPhone": userName,
      "state": 3
    }, success: (json) {
      print(json);
      resList = json;
      if(resList.isNotEmpty){
        newApplyLength = resList.length;
      }
      notifyListeners();
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {});
  }

  /// 添加好友弹窗
  alertDialog(BuildContext context) async {
    var alertDialogs = await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("请输入对方手机号"),
            content: TextField(
              controller: contactsController,
            ),
            actions: <Widget>[
              TextButton(
                  child: const Text("取消"),
                  onPressed: () {
                    contactsController.clear();
                    Navigator.pop(context, "cancel");
                  }),
              TextButton(
                  child: const Text("确定"),
                  onPressed: () {
                    if (!isPhoneLegal(contactsController.text)) {
                      Fluttertoast.showToast(
                          msg: '请输入正确格式的手机号',
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          fontSize: 16.0);
                    } else {
                      _sendToServer();
                      contactsController.clear();
                      Navigator.pop(context, "yes");
                    }
                  }),
            ],
          );
        });
    return alertDialogs;
  }

  /// 判断输入的是否是手机号
  bool isPhoneLegal(String str) {
    return RegExp(
            r'^((13[0-9])|(14[0-9])|(15[0-9])|(16[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\d{8}$')
        .hasMatch(str);
  }

  /// 服务器请求
  _sendToServer() {
    ResponseResults responseResults;
    Http().post(Urls.ADD_CONTACTS, {}, data: {
      "phone": userName,
      "contactsPhone": contactsController.text,
    }, success: (json) {
      responseResults = ResponseResults.fromJson(json);
      if (responseResults.code == 1) {
        Fluttertoast.showToast(
            msg: '好友申请已发送',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            fontSize: 16.0);
        print(responseResults.message);
      } else {
        Fluttertoast.showToast(
            msg: responseResults.message.toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            fontSize: 16.0);
      }
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {});
  }

}
