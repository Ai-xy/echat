import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:e_chart_mvvm/model/sqlite_user.dart';
import 'package:e_chart_mvvm/model/user.dart';
import 'package:e_chart_mvvm/view/chart_page.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:signalr_client/hub_connection.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class UserInfoViewModel extends BaseViewModel {
  HubConnection? mConnection;
  double extraPicHeight = 0; // 初始化要加载到图片上的高度
  BoxFit fitType = BoxFit.fitWidth; // 图片填充类型（刚开始滑动时是以宽度填充，拉开之后以高度填充）
  double prev_dy = 0; // 前一次手指所在处的y值
  double init_dy = 0; // 手指第一次点击屏幕的位置
  User userInfo = User(
      phone: "13668243979",
      nickName: '创驰蓝天',
      sex: '男',
      location: '四川 成都',
      sign: '春风不解风情，吹动少年的心',
      selectedPhotos: 'img/atnw.jpg',
      age: '21 岁');
  User? sUser; // 传过来的user

  bool isPointerUp = false; // 判断手指是否抬起
  String? dbUserName;

  ScrollController? controller;

  // 标题渐变
  int trans = 0;

  AnimationController? animationController;
  Animation<double>? anim;

  /// 初始化
  init(AnimationController? mController, Animation<double>? mAnim, User user,
      HubConnection? connection) {
    mConnection = connection;
    sUser = user;
    animationController = mController;
    anim = mAnim;
    //_getUserPwdFromDb();
    getUser();
    controller = ScrollController();
    controller!.addListener(() {
      if (controller!.position.pixels.toInt() <= 200) {
        trans = controller!.position.pixels.toInt();
        notifyListeners();
      } else {
        trans = 255;
        notifyListeners();
      }
    });
  }

  /// 获取用户信息
  getUser() {
    User user;
    Http().post(Urls.GET_USER_INFO, {}, data: {
      "phone": sUser!.phone,
    }, success: (json) {
      user = User.fromJson(json);
      print(json);
      if (user.state == 1) {
        userInfo = user;
        userInfo.phone ??= "13668243979";
        userInfo.nickName ??= '创驰蓝天';
        userInfo.sex ??= '男';
        userInfo.location ??= '四川 成都';
        userInfo.sign ??= '春风不解风情，吹动少年的心';
        userInfo.selectedPhotos ??= 'img/atnw.jpg';
        userInfo.age ??= '21 岁';
        notifyListeners();
      }
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {});
    notifyListeners();
  }

  /// 图片伸缩
  updatePicHeight(changed) {
    if (prev_dy == 0) {
      prev_dy = changed;
    }

    if (extraPicHeight >= 45) {
      fitType = BoxFit.fitHeight;
    } else {
      fitType = BoxFit.fitWidth;
    }

    if (changed - prev_dy > 0) {
      // 只有向下滑动才执行缩放效果
      if (controller!.position.pixels.toInt() == 0) {
        // 在列表到顶时执行
        if (extraPicHeight < 80) {
          extraPicHeight += changed - prev_dy; //新的一个y值减去前一次的y值然后累加，作为加载到图片上的高度。

        } else {
          extraPicHeight += (changed - prev_dy) / 5;
        }
        prev_dy = changed;
        notifyListeners();
      } else {
        extraPicHeight = 0;
        prev_dy = 0;
        changed = 0;
      }
    }
  }

  ///设置动画让extraPicHeight的值从当前的值渐渐回到 0
  runAnimate() {
    anim = Tween(begin: extraPicHeight, end: 0.0).animate(animationController!)
      ..addListener(() {
        if (extraPicHeight >= 45) {
          fitType = BoxFit.fitHeight;
        } else {
          fitType = BoxFit.fitWidth;
        }
        extraPicHeight = anim!.value;
        fitType = fitType;
        notifyListeners();
      });
    prev_dy = 0;
    notifyListeners();
  }

  /// 跳转发送消息页面
  toChatPage(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChartPage(
                  user: sUser!,
                  connection: mConnection,
                )));
  }

  alterUserInfo(){

  }


  /// 从本地数据库中获得用户信息
  _getUserPwdFromDb() async {
    TableReUserProvider tableTestProvider = TableReUserProvider();
    var databasePath = await getDatabasesPath();
    print(databasePath);
    String path = join(databasePath, "userInfo.db");
    Database db = await openDatabase(path);

    //查询数据，获取用户名和密码
    RegisterUser dd = await tableTestProvider.getTodo(1, path, db);
    print('登录界面用户信息:${dd.toMap()}');
    dbUserName = dd.name!;
    tableTestProvider.close(db);
  }
}
