import 'dart:math';
import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:e_chart_mvvm/model/chart_content_model.dart';
import 'package:e_chart_mvvm/model/chat_content_single_model.dart';
import 'package:e_chart_mvvm/model/send_message_template_model.dart';
import 'package:e_chart_mvvm/model/user.dart';
import 'package:e_chart_mvvm/view/message_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signalr_client/hub_connection.dart';

class MessageViewModel extends BaseViewModel {
  int pageState = 0;
  int scrollerState = 0;
  List<int> unreadMessagesNum = [];
  bool isMove = false;
  ScrollController msgListController = ScrollController();
  User? mUser;
  String? userName;
  String? temp;
  ChatCountModel? mMessage; // 收到的数据
  // List<ChatCountModel> chats = [];
  List<ChatContentSingleModel> chats = [];
  List<List<SendMessageTemplateModel>> testList = [];
  List<SendMessageTemplateModel> chatsMesList = [];
  List<SendMessageTemplateModel> temMessageList = []; // 用于刷新判断
  List<User>? contactsList = []; // 好友列表
  List<User>? users = [];
  HubConnection? mConnection;
  var slideKey;

  /// 初始化
  init(HubConnection? connection, List<SendMessageTemplateModel>? messageList) {
    print('初始化');
    mConnection = connection;
    chatsMesList = messageList!;

//    for (var item in data) {
//      users!.add(
//        User(
//            avatarUrl: item["avatarUrl"],
//            id: 2,
//            nickName: item["userName"],
//            sex: item["sex"]),
//      );
//    }
//    //users = users!.getRange(0, data.length >= 3 ? 3 : data.length - 1).toList();
//    for (var item in users!) {
//      List<User> userIds = <User>[]; //userIds和谁谈话
//      userIds.add(item);
//      chats.add(ChatContentSingleModel(
//          isRead: false,
//          lastContent: '最后一条数据',
//          lastUpdateTime: genRandomTime(),
//          user: userIds[0]));
//    }
    getUser();
  }

  /// 获得数据
  getData(List<SendMessageTemplateModel>? message) {
    print('getData');
//    users = <User>[];
//    for (var item in message!) {
//      users!.add(
//        item.fromWho!
//      );
//    }
//
//    for (var item in users!) {
//      List<User> userIds = <User>[]; //userIds和谁谈话
//      userIds.add(item);
//      chats.add(ChatCountModel(
//          isRead: false,
//          lastContent: '最后一条数据',
//          lastUpdateTime: genRandomTime(),
//          user: userIds));
//    }

    // 初始化list
    for (int i = 0; i < contactsList!.length; i++) {
      testList.add([]);
    }
//    print("testList0:" + testList.toString());
    int itemLength = 0;
    bool isFind = false;
    // 每行一代表一个好友
    for (int i = 0; i < contactsList!.length; i++) {
      for (int j = 0; j < message!.length; j++) {
        if (contactsList![i].phone == message[j].fromWho!.phone) {
          testList[itemLength].add(message[j]);

//          print(contactsList![i].phone.toString() +
//              " $j " +
//              message[j].fromWho!.phone.toString());
          isFind = true;
        }
      }
      if (isFind) {
        itemLength++;
        isFind = false;
      }
    }

//    print("testList:" + testList.toString());
//    print("item长度: $itemLength");

    for (int i = 0; i < itemLength; i++) {
      chats.add(ChatContentSingleModel(
          isRead: false,
          lastContent: "000",
          lastUpdateTime: genRandomTime(),
          user: User(nickName: '0000000')));
      unreadMessagesNum.add(testList[i].length);
    }
    for (int i = 0; i < itemLength; i++) {
      for (var item in testList[i]) {
        print("i: $i" + item.message.toString());
        chats[i] = ChatContentSingleModel(
            isRead: false,
            lastContent: item.message,
            lastUpdateTime: genRandomTime(),
            user: item.fromWho!);
      }
    }

////    print('testList,len'+testList.length.toString());
//    int temNum = message!.length;
//    print("temNum0"+temNum.toString());
//    if(contactsList!.length<message.length){
//      temNum = contactsList!.length;
//      print("temNum1"+temNum.toString());
//    }
//
//    for (int i = 0; i < testList.length; i++) {
//      for (int j = 0; j < contactsList!.length; j++) {
//        if (contactsList![j].phone == testList[i][j].fromWho!.phone) {
//          testList[i][j]=message[j];
//        }
//      }
//
//      for(int i=0;i<testList.length;i++){
//        print(testList[i][0].message);
//      }

//      chats.add(ChatContentSingleModel(
//          isRead: false,
//          lastContent: "000",
//          lastUpdateTime: genRandomTime(),
//          user: User(nickName: '0000000')));

//    for (var item in message!) {
//      for (int i = 0; i < contactsList!.length; i++) {
////        chats.add(ChatContentSingleModel(
////            isRead: false,
////            lastContent: item.message,
////            lastUpdateTime: genRandomTime(),
////            user: item.fromWho!));
//        if (item.fromWho!.phone == contactsList![i].phone) {
//          chats[i] = ChatContentSingleModel(
//              isRead: false,
//              lastContent: item.message,
//              lastUpdateTime: genRandomTime(),
//              user: item.fromWho!);
//          print("chats.length" + chats.length.toString());
//        }
//      }
//    }
    notifyListeners();
//    mMessage = message;

//
//    chats = <ChatCountModel>[];
//    users = <User>[];
//    for (var item in data) {
//      users!.add(
//        User(
//            avatarUrl: item["avatarUrl"],
//            id: 2,
//            nickName: item["userName"],
//            sex: item["sex"]),
//      );
//    }
//
//    //users = users!.getRange(0, data.length >= 3 ? 3 : data.length - 1).toList();
//    for (var item in users!) {
//      List<User> userIds = <User>[]; //userIds和谁谈话
//      userIds.add(item);
//      chats!.add(ChatCountModel(
//          isRead: false,
//          lastContent: '最后一条数据',
//          lastUpdateTime: genRandomTime(),
//          userId: userIds));
//    }
  }

//  DateTime date = DateTime.parse(item.sendTime!);
//  print(date);
  /// 刷新
  refresh(List<SendMessageTemplateModel>? message) {
    chats = [];
    print('刷新');
    for (int i = 0; i < contactsList!.length; i++) {
      testList[i] = [];
    }
    int itemLength = 0;
    bool isFind = false;

    // 每行一代表一个好友
    for (int i = 0; i < contactsList!.length; i++) {
      for (int j = 0; j < message!.length; j++) {
        if (contactsList![i].phone == message[j].fromWho!.phone) {
          testList[itemLength].add(message[j]);
          print(contactsList![i].phone.toString() +
              " $j " +
              message[j].fromWho!.phone.toString());
          isFind = true;
        }
      }
      if (isFind) {
        itemLength++;
        isFind = false;
      }
    }
    print("item长度: $itemLength");
    for (int i = 0; i < itemLength; i++) {
      chats.add(ChatContentSingleModel(
          isRead: false,
          lastContent: "000",
          lastUpdateTime: genRandomTime(),
          user: User(nickName: '0000000')));
    }
    print(chats.length);

    for (int i = 0; i < itemLength; i++) {
      print("testList[i].length: " + testList[i].toString());
      for (var item in testList[i]) {
        print("i: $i" + item.message.toString());
        chats[i] = ChatContentSingleModel(
            isRead: false,
            lastContent: item.message,
            lastUpdateTime: genRandomTime(),
            user: item.fromWho!);
      }
    }
  }

  /// 获取用户名
  getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userName = prefs.getString('user_name');
    this.userName = userName;
    print(this.userName);
  }

  /// 获取用户信息
  getUser() async {
    await getUserName();
    User user;
    Http().post(Urls.GET_USER_INFO, {}, data: {
      "phone": userName,
    }, success: (json) {
      user = User.fromJson(json);
      print(json);
      if (user.state == 1) {
        mUser = user;
        print(mUser);
        notifyListeners();
      }
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {
      getContacts();
    });
    notifyListeners();
  }

  /// 获取好友列表
  getContacts() async {
    await getUserName();
    User user;
    print('获取好友列表');
    List<dynamic> resList;
    Http().post(Urls.GET_ADD_CONTACT, {},
        data: {"contactsPhone": userName, "state": 2}, success: (json) {
      print(json);
      resList = json;
      for (int i = 0; i < resList.length; i++) {
        user = User.fromJson(resList[i]);
        contactsList!.add(user);
      }
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {
      getData(chatsMesList);
//      if (contactsList != null) {
//        for (int i = 0; i < contactsList!.length; i++) {
//          if (mMessage!.user!.phone == contactsList![i].phone) {
//            chats.remove(i);
//            print('list 长度：' + chats.length.toString());
//            chats.insert(
//                i,
//                ChatCountModel(
//                  isRead: false,
//                  user: mMessage!.user,
//                  lastContent: mMessage!.lastContent,
//                  lastUpdateTime: genRandomTime(),
//                ));
//          }
//        }
//        notifyListeners();
//      }
    });
  }

  /// 生成随机时间
  String genRandomTime() {
    int rand = Random().nextInt(1000000000); //从1970年起算的毫秒数
    DateTime now = DateTime.now();
    DateTime randTime =
        DateTime.fromMillisecondsSinceEpoch(now.millisecondsSinceEpoch - rand);
    Duration diff = now.difference(randTime); // 计算时差
    if (diff.inDays > 0) {
      return "${diff.inDays}天前";
    } else if (diff.inHours > 0 && diff.inDays == 0) {
      return "${diff.inHours}小时前";
    } else if (diff.inHours == 0 && diff.inDays == 0 && diff.inMinutes > 5) {
      return "${diff.inMinutes}分前";
    } else {
      return "刚刚";
    }
  }

  /// 空页面判断
  void pageStateJudge() {
    if (chats.isEmpty) {
      pageState = 0;
    } else {
      pageState = 1;
    }
  }

  /// 页面构建
  Widget buildPage(BuildContext context, var model) {
    slideKey = null;
    isMove = false;
    pageStateJudge();
    print('pageState' + pageState.toString());
    switch (pageState) {
      case 0:
        return nullPage(context);
      case 1:
        return buildNewsList(model, context);
    }
    return Container();
  }

  /// 删除列表子项
  void removeItem(int index) {
    slideKey = null;
    chats.removeAt(index);
    notifyListeners();
  }

  List data = [
    {
      "userId": "00001",
      "phone": "1366595",
      "userName": "爱丽丝",
      "sex": "女",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl": "img/head_portrait.png"
    },
    {
      "userId": "00002",
      "userName": "张三",
      "sex": "女",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl": "img/head_portrait.png"
    },
    {
      "userId": "00003",
      "userName": "懂王",
      "sex": "女",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl": "img/head_portrait.png"
    },
    {
      "userId": "00004",
      "userName": "美国总统特朗普",
      "sex": "女",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl": "img/head_portrait.png"
    },
    {
      "userId": "00005",
      "userName": "懂哥",
      "sex": "男",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl": "img/head_portrait.png"
    }
  ];
}
