/// 底部导航栏

import 'dart:convert';

import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:e_chart_mvvm/model/chat_content_single_model.dart';
import 'package:e_chart_mvvm/model/response_results.dart';
import 'package:e_chart_mvvm/model/send_message_template_model.dart';
import 'package:e_chart_mvvm/model/user.dart';
import 'package:e_chart_mvvm/view/building_page.dart';
import 'package:e_chart_mvvm/view/contacts_page.dart';
import 'package:e_chart_mvvm/view/message_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:signalr_client/hub_connection.dart';
import 'package:signalr_client/hub_connection_builder.dart';

class MainViewModel extends BaseViewModel {
  HubConnection? connection;
  String? connId;
  String? userPhone;
  SendMessageTemplateModel? tempMes;
  User userInfo = User();
  int currentIndex = 0;
  List<Widget> pages = [];
  List<SendMessageTemplateModel> messageList = [];

  MainViewModel() {
    print('构造方法');
  }

  /// 初始化
  init(String phone) {
    userInfo.nickName = '用户001';
    userPhone = phone;
    getUserInfo();
  }

  /// 页面构建
  Widget buildPage(BuildContext context, MainViewModel model) {
    print('重构');
    pages.clear();
    pages.add(tempMes == null
        ? const Center(
            child: Text('暂无消息~'),
          )
        : MessagePage(
            connection: connection,
            messageList: messageList,
//            message: ChatContentSingleModel(
//                user: tempMes!.fromWho, lastContent: tempMes!.message),
          ));
    pages.add(ContactsPage(connection: connection));
    pages.add(const BuildingPage());
    return Container();
  }

  /// 获取connId
  getConnId() async {
    connection = HubConnectionBuilder()
        .withUrl('http://169.254.10.233:5000/chatHub')
        .build();
    connection!.start(); //调用服务器端OnConnectedAsync()方法，返回一个id
    connection!.on('receviceConnId', (message) {
      //list形式返回
      connId = message.first.toString();
      sendConnIdToServer(connId!); // 上传到服务器
      print('connId：' + connId.toString());
      connection!.on('receiveMsg', (message) {
        print(message);
        print(jsonEncode(message.first));

        Map<String, dynamic> map = jsonDecode(jsonEncode(message.first));
        print(map);

        Map<String, dynamic> userFromWho =
            jsonDecode(jsonEncode(map["fromWho"]));
        print(userFromWho);

        Map<String, dynamic> userToWho = jsonDecode(jsonEncode(map["toWho"]));
        print(userToWho);

        SendMessageTemplateModel sendMessageTemplateModel =
            SendMessageTemplateModel(
                fromWho: User.fromJson(userFromWho),
                toWho: User.fromJson(userToWho),
                message: map["message"],
                sendTime: map["sendTime"]);

        tempMes = sendMessageTemplateModel;
//        print("接收到的message：" + sendMessageTemplateModel.fromWho.toString());
//        print("接收到的message：" + sendMessageTemplateModel.toWho.toString());
        print("接收到的message：" + sendMessageTemplateModel.message!);

        messageList.add(sendMessageTemplateModel);

        notifyListeners();
      });
      notifyListeners();
    });
  }

  /// 获取用户信息
  Future getUserInfo() async {
    User user;
    Http().post(Urls.GET_USER_INFO, {}, data: {
      "phone": userPhone,
    }, success: (json) {
      user = User.fromJson(json);
      print(json);
      print(user);
      userInfo = user;
      print('0/////////////////////////' + userInfo.nickName.toString());
      notifyListeners();
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {
      getConnId();
    });
    notifyListeners();
  }

  /// 上传connID
  sendConnIdToServer(String cId) async {
    Http().post(Urls.BUILD_CONNECTION, {},
        data: {"phone": userPhone, "sign": 0, "connId": cId}, success: (json) {
      print(json);
      ResponseResults results = ResponseResults.fromJson(json);
      Fluttertoast.showToast(
          msg: results.message.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      notifyListeners();
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {});
  }
}
