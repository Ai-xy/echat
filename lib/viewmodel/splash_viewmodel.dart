import 'dart:async';
import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/common/Routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashViewModel extends BaseViewModel {
  /// 初始化
  init(BuildContext context) {
    Future.delayed(const Duration(seconds: 2), () {
      startTime(context);
    });
  }

  startTime(BuildContext context) async {
    // 判断是否是第一次使用app
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? firstTime = prefs.getBool('first_time');

    var _duration = const Duration(seconds: 3);

    if (firstTime != null && firstTime) {
      return Timer(_duration, () {
        Navigator.of(context)
            .pushReplacementNamed(Routes.login_page);
      });
    } else {
      // 第一次使用
      prefs.setBool('first_time', true);
      return Timer(_duration, () {
        Navigator.of(context).pushReplacementNamed(Routes.register_page);
      });
    }
  }
}
