// ignore_for_file: file_names

import 'dart:math';

import 'package:e_chart_mvvm/model/chart_content_model.dart';
import 'package:e_chart_mvvm/model/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:uuid/uuid.dart';

class ChatListProvider with ChangeNotifier {
  List<ChatCountModel> chats = <ChatCountModel>[];
  List<User> users = <User>[];
  ChatListProvider() {
    data.forEach((item) {
      users.add(
        User(
            avatarUrl: item["avatarUrl"],
            id: 3,
            nickName: item["userName"],
            sex: item["sex"]),
      );
    });

    users.forEach((item) {
      List<User> userIds = <User>[]; //userIds和谁谈话
      userIds.add(item);
      chats.add(ChatCountModel(
          isRead: false,
          lastContent: '最后一条数据',
          lastUpdateTime: genRandomTime(),
          user: userIds));
    });
  }

  /// 生成随机时间
  String genRandomTime() {
    int rand = Random().nextInt(100000); //从1970年起算的毫秒数
    DateTime now = DateTime.now();
    DateTime randTime =
        DateTime.fromMicrosecondsSinceEpoch(now.microsecondsSinceEpoch - rand);
    return randTime.toString();
  }

  List data = [
    {
      "userId": "00001",
      "userName": "爱丽丝",
      "sex": "女",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl":"Img/head_portrait.png"
    },
    {
      "userId": "00002",
      "userName": "张三",
      "sex": "女",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl":"Img/head_portrait.png"
    },
    {
      "userId": "00003",
      "userName": "懂王",
      "sex": "女",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl":"Img/head_portrait.png"
    },
    {
      "userId": "00004",
      "userName": "美国总统特朗普",
      "sex": "女",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl":"Img/head_portrait.png"
    },
    {
      "userId": "00005",
      "userName": "懂哥",
      "sex": "男",
      "sign": "阿八八八",
      "location": "四川 成都",
      "selectedPhotos": "xxx",
      "age": "20",
      "birthday": "0210",
      "avatarUrl":"Img/head_portrait.png"
    }
  ];
}
