// ignore_for_file: file_names

import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/model/chatRecord.dart';
import 'package:signalr_client/hub_connection.dart';
import 'package:signalr_client/hub_connection_builder.dart';

class SignalRViewModel extends BaseViewModel {
  HubConnection? connection;
  String? connId;
  List<ChatRecord>? record;

  SignalRViewModel() {
    print('执行构造函数');
    record = [];
    record!.add(ChatRecord(
        avatarUrl: 'img/head_portrait.png', sender: 0, content: '哈哈哈'));
    record!.add(ChatRecord(
        avatarUrl: 'img/head_portrait.png', sender: 1, content: 'xswl'));
    record!.add(ChatRecord(
        avatarUrl: 'img/head_portrait.png', sender: 0, content: 'shit bro!'));
    record!.add(ChatRecord(
        avatarUrl: 'img/head_portrait.png', sender: 1, content: 'emmm'));

    //.net core默认端口http://localhost:5000，但要申明路径名chatHub创建监听通道
    connection = HubConnectionBuilder()
        .withUrl('https://169.254.10.233:5001/chatHub')
        .build();
    connection!.start(); //调用服务器端OnConnectedAsync()方法，返回一个id

    connection!.on('receviceConnId', (message) {
      //list形式返回
      connId = message.first.toString();
      print('connId：'+connId.toString());
      notifyListeners();
    });

    //接收消息
    connection!.on('receiveMsg', (message) {
      print(message);
      ///todo message.first.toString()
      record!.add(ChatRecord(
          content: message.first.toString(),
          avatarUrl: 'img/atnw.jpg',
          sender: 0));
      notifyListeners();
    });
  }

//  /// 发送消息
//  sendMessage(msg) {
//    // invoke 调用服务器中的哪个方法
//    connection!.invoke('receiveMsg', args: [
//      jsonEncode(
//          SendMessageTemplateModel(fromWho: connId, toWho: '', message: msg)
//              .toJson())
//    ]);
//    record!.add(
//        ChatRecord(content: msg, avatarUrl: 'img/head_portrait', sender: 0));
//    notifyListeners();
//  }
}
