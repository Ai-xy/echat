import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Routes.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:e_chart_mvvm/model/response_results.dart';
import 'package:e_chart_mvvm/model/sqlite_user.dart';
import 'package:e_chart_mvvm/view/main_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class LoginViewModel extends BaseViewModel {
  bool pswVisible = true;
  var phoneController = TextEditingController();
  var pwdController = TextEditingController();
  String dbUserName = '';
  String dbUserKey = '';
  bool checkboxSelected = false;

  /// 初始化
  init() {
    _getCheckboxSelected();
    phoneController.clear();
    pwdController.clear();
    phoneController.addListener(() {});
    pwdController.addListener(() {});
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
  }

  /// 销毁
  @override
  void dispose() {
    super.dispose();
    phoneController.clear();
    pwdController.clear();
  }

  /// 获取记住密码选择框状态
  _getCheckboxSelected() async {
    //await _getUserPwdFromDb();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? isCheck = prefs.getBool('checkboxSelected');
    print(isCheck);
    if (isCheck != null && isCheck) {
      checkboxSelected = true;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? userPwd = prefs.getString('user_pwd');
      String? userName = prefs.getString('user_name');
      print(userPwd! + userName!);
      phoneController.text = userName;
      pwdController.text = userPwd;
      notifyListeners();
    }
  }

  /// 判断输入的是否是手机号
  bool isPhoneLegal(String str) {
    return RegExp(
            r'^((13[0-9])|(14[0-9])|(15[0-9])|(16[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\d{8}$')
        .hasMatch(str);
  }

  /// 改变密码是否可见状态
  changeEyeState() {
    pswVisible = !pswVisible;
    notifyListeners();
  }

  /// 改变记住密码勾选框状态
  changeBoxSelectedState() {
    checkboxSelected = !checkboxSelected;
    notifyListeners();
  }

  /// 判断账号密码是否正确
//  getUserNameAndPwd(BuildContext context) async {
//    if (dbUserName != phoneController.text) {
//      Fluttertoast.showToast(
//          msg: '账号不存在',
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.BOTTOM,
//          fontSize: 16.0);
//    } else if (dbUserKey != pwdController.text) {
//      Fluttertoast.showToast(
//          msg: '密码错误，请重新输入',
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.BOTTOM,
//          fontSize: 16.0);
//    } else {
//      Fluttertoast.showToast(
//          msg: '登录成功',
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.BOTTOM,
//          fontSize: 16.0);
//      saveUserName(phoneController.text);
//      saveUserPwd(pwdController.text);
//      saveCheckboxSelected(checkboxSelected);
//      Navigator.of(context).pushNamed(Routes.main_page);
//    }
//  }

  /// 保存用户名至SP
  saveUserName(String str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_name', str);
  }

  /// 保存密码至SP
  saveUserPwd(String str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_pwd', str);
  }

  /// 保存记住密码选择框状态至SP
  saveCheckboxSelected(bool str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('checkboxSelected', str);
  }

  /// 登陆
  login(BuildContext context, AnimationController controller) {
    print('登录');
    bool isOk = isPhoneLegal(phoneController.text);
    if (phoneController.text.isEmpty || pwdController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: '账号或密码为空',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
    } else {
      if (isOk) {
        controller.forward();
        sendToServer(context);
      } else {
        Fluttertoast.showToast(
            msg: "请输入正确的手机号",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            fontSize: 16.0);
      }
    }
  }

  /// 提交到服务器
  sendToServer(BuildContext context) {
    ResponseResults responseResults;
    Http().post(Urls.LOGIN, {}, data: {
      "phone": phoneController.text,
      "password": pwdController.text,
      "state": 1
    }, success: (json) {
      responseResults = ResponseResults.fromJson(json);
      Fluttertoast.showToast(
          msg: responseResults.message.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      if (responseResults.code == 1) {
        print(responseResults.message);
        saveUserName(phoneController.text);
        saveUserPwd(pwdController.text);
        saveCheckboxSelected(checkboxSelected);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MainPage(
                      phone: phoneController.text,
                    )));
      }
    }, fail: (reason, statusCode) {
      print('失败');
      Fluttertoast.showToast(
          msg: '未知错误',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {});
    notifyListeners();
  }

  /// 跳转至注册页面
  toRegisterPage(BuildContext context) {
    Navigator.of(context).pushNamed(Routes.register_page);
  }

  /// 从本地数据库中获得用户信息
  _getUserPwdFromDb() async {
    TableReUserProvider tableTestProvider = TableReUserProvider();
    var databasePath = await getDatabasesPath();
    print(databasePath);
    String path = join(databasePath, "userInfo.db");
    Database db = await openDatabase(path);

    //查询数据，获取用户名和密码
    RegisterUser dd = await tableTestProvider.getTodo(1, path, db);
    print('登录界面用户信息:${dd.toMap()}');
    dbUserName = dd.name!;
    dbUserKey = dd.pwd!;
    tableTestProvider.close(db);
  }
}
