import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:e_chart_mvvm/model/response_results.dart';
import 'package:e_chart_mvvm/model/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NewContactsViewModel extends BaseViewModel {
  String? userName;
  List<User> newContactsList = [];

  init() {
    getAddContactsApply();
  }

  /// 获取用户名
  getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userName = prefs.getString('user_name');
    this.userName = userName;
    print(this.userName);
  }

  /// 获取好友申请列表
  getAddContactsApply() async {
    await getUserName();
    User user;
    print('获取好友申请列表');
    List<dynamic> resList;
    Http().post(Urls.GET_ADD_CONTACT, {}, data: {
      "contactsPhone": userName,
      "state": 3
    }, success: (json) {
      print(json);
      resList = json;
      for (int i = 0; i < resList.length; i++) {
        user = User.fromJson(resList[i]);
        newContactsList.add(user);
        notifyListeners();
      }
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {});
  }

  /// 通过联系人申请
  passTheContactsApply(int i) {
    Http().post(Urls.CONFIRM_Add_CONTACTS, {}, data: {
      "phone": userName,
      "contactsPhone": newContactsList[i].phone,
      "state": 1
    }, success: (json) {
      print(json);
      ResponseResults results = ResponseResults.fromJson(json);
      Fluttertoast.showToast(
          msg: results.message.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      newContactsList.removeAt(i);
      notifyListeners();
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {});
  }

  /// 删除好友申请弹窗
  alertDialog(BuildContext context) async {
    var alertDialogs = await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("是否删除这条好友申请"),
            actions: <Widget>[
              TextButton(
                  child: const Text("取消"),
                  onPressed: () {
                    Navigator.pop(context, "cancel");
                  }),
              TextButton(child: const Text("确定"), onPressed: () {}),
            ],
          );
        });
    return alertDialogs;
  }
}
