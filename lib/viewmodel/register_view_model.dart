import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/model/response_results.dart';
import 'package:e_chart_mvvm/model/sqlite_user.dart';
import 'package:e_chart_mvvm/view/register_nickName_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:e_chart_mvvm/common/Urls.dart';

class RegisterViewModel extends BaseViewModel {
  bool pswVisible = true;
  var phoneController = TextEditingController();
  var pwdController = TextEditingController();
  var pwdControllerAgain = TextEditingController();

  init() {
    phoneController.clear();
    pwdController.clear();
    phoneController.addListener(() {});
    pwdController.addListener(() {});
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
  }

  @override
  dispose() {
    super.dispose();
    phoneController.clear();
    pwdController.clear();
    pwdControllerAgain.clear();
  }

  /// 注册
  register(BuildContext context) {
    bool isOk = isPhoneLegal(phoneController.text);
    if (phoneController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: '请输入手机号',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
    } else if (pwdController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: '请输入密码',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
    } else if (pwdControllerAgain.text.isEmpty) {
      Fluttertoast.showToast(
          msg: '请再次输入密码',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
    } else {
      if (!isOk) {
        Fluttertoast.showToast(
            msg: '输入的手机号格式不对',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            fontSize: 16.0);
      } else {
        if (pwdController.text == pwdControllerAgain.text) {
          sendToServer(context);
        } else {
          Fluttertoast.showToast(
              msg: '请输入两次相同的密码',
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              fontSize: 16.0);
        }
      }
    }
  }

  /// 提交到服务器
  sendToServer(BuildContext context) {
    ResponseResults responseResults;
    Http().post(Urls.USER_REGISTER, {}, data: {
      "phone": phoneController.text,
      "password": pwdController.text,
      "state": 1
    }, success: (json) {
      responseResults = ResponseResults.fromJson(json);
      Fluttertoast.showToast(
          msg: responseResults.message.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      if (responseResults.code == 1) {
        // 储存到数据库
        _addUserToSq(phoneController.text, pwdController.text, context);
        print(responseResults.message);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => RegisterNickNamePage(
                      userPhone: phoneController.text,
                    )));
      }
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {});
  }

  /// 改变密码是否可见状态
  changeEyeState() {
    pswVisible = !pswVisible;
    notifyListeners();
  }

  /// 判断输入的是否是手机号
  bool isPhoneLegal(String str) {
    return RegExp(
            r'^((13[0-9])|(14[0-9])|(15[0-9])|(16[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\d{8}$')
        .hasMatch(str);
  }

  /// 标识已经注册过账号
  save() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('first_time', true);
  }

  /// 保存用户名
  saveUserName(String str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_name', str);
  }

  /// 保存密码
  saveUserPwd(String str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_pwd', str);
  }

  /// 将用户名和密码储存到数据库中
  _addUserToSq(String userName, String userKey, BuildContext context) async {
    TableReUserProvider tableTestProvider = TableReUserProvider();
    var databasePath = await getDatabasesPath();
    print(databasePath);
    String path = join(databasePath, "userInfo.db");
    await deleteDatabase(path);
    print('先删除数据量');
    await tableTestProvider.open(path);
    print('重建数据库');

    //插入第一条数据
    RegisterUser registerUser = RegisterUser();
    registerUser.id = 1;
    registerUser.name = userName;
    registerUser.pwd = userKey;
    print('插入之前');
    try {
      RegisterUser td = await tableTestProvider.insert(registerUser);
      print('插入:${td.toMap()}');
      Fluttertoast.showToast(
          msg: '注册成功',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      // 查询数据
      RegisterUser dd = await tableTestProvider.getTodo2(1);
      print('todo:${dd.toMap()}');
      await tableTestProvider.close2();
      await save();

    } catch (e) {
      print('错误');
    }
  }
}
