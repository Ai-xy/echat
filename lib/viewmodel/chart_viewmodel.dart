import 'dart:convert';
import 'package:e_chart_mvvm/base/base_viewmodel.dart';
import 'package:e_chart_mvvm/base/http/http.dart';
import 'package:e_chart_mvvm/common/Urls.dart';
import 'package:e_chart_mvvm/model/chatRecord.dart';
import 'package:e_chart_mvvm/model/chat_logs.dart';
import 'package:e_chart_mvvm/model/send_message_template_model.dart';
import 'package:e_chart_mvvm/model/sqlite_chatList.dart';
import 'package:e_chart_mvvm/model/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signalr_client/hub_connection.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class ChartViewModel extends BaseViewModel {
  User? user;
  User? contact;
  Color sendButtonColor = const Color(0xffd1f3ff);
  bool ifNewMsg = true;
  bool isWord = false;
  bool ifSaveLogs = false;
  ScrollController msgListController = ScrollController();
  TextEditingController sendController = TextEditingController();
  TableChatListProvider tableChatListProvider = TableChatListProvider();

  HubConnection? connection;

  List<SendMessageTemplateModel>? message; // 消息列表
  String? connId;
  String? userName;
  List<ChatRecord>? messageList = [];
  List<ChatRecord>? record = [];

  ChartViewModel(HubConnection connection, User user) {
    print('执行构造函数');
//    record!.add(ChatRecord(
//        avatarUrl: 'img/head_portrait.png', sender: 0, content: '哈哈哈'));
//    record!.add(ChatRecord(
//        avatarUrl: 'img/head_portrait.png', sender: 1, content: 'xswl'));
//    record!.add(ChatRecord(
//        avatarUrl: 'img/head_portrait.png', sender: 0, content: 'shit bro!'));
//    record!.add(ChatRecord(
//        avatarUrl: 'img/head_portrait.png', sender: 1, content: 'emmm'));

    //.net core默认端口http://localhost:5000，但要申明路径名chatHub创建监听通道

//    connection = HubConnectionBuilder()
//        .withUrl('http://169.254.10.233:5000/chatHub')
//        .build();
//    connection!.start(); //调用服务器端OnConnectedAsync()方法，返回一个id
//
//    connection!.on('receviceConnId', (message) {
//      //list形式返回
//      connId = message.first.toString();
//      print('connId：' + connId.toString());
//      notifyListeners();
//    }
//    );

    //接收消息

//    connection.on('receiveMsg', (message) {
//      print(message);
//      Map<String, dynamic> map = jsonDecode(jsonEncode(message.first));
//      SendMessageTemplateModel sendMessageTemplateModel =
//      SendMessageTemplateModel(
//          message: map["message"]);
//      ///todo message.first.toString()
//      record!.add(ChatRecord(
//          content: sendMessageTemplateModel.message,
//          avatarUrl: 'img/atnw.jpg',
//          sender: 0));
//      ifNewMsg = false;
//      messageList = record;
//      notifyListeners();
//    });
  }

  /// 初始化
  init(BuildContext context, User contact, HubConnection? connection,
      List<SendMessageTemplateModel>? message) {
    this.contact = contact;
    this.connection = connection;
    this.message = message;
    getContactsConnId();
    sendController.addListener(() {
      isEmpty(context);
    });
    getChatLogsFromDb(contact.phone);
  }

  /// 获取用户名
  getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userName = prefs.getString('user_name');
    this.userName = userName;
    print(this.userName);
  }

  /// 获得数据
  getData(List<SendMessageTemplateModel> message) {
    print('getData');
//    if (ifSaveLogs) {
//      getChatLogsFromDb(contact!.phone);
//      ifSaveLogs = false;
//    }

//    for (int i = 0; i < message.length; i++) {
//      messageList!.add(ChatRecord(
//          content: message[i].message, avatarUrl: 'img/atnw.jpg', sender: 0));
//    }
  }

  /// 销毁
  @override
  void dispose() {
    super.dispose();
    print('销毁');
    sendController.dispose();
    msgListController.dispose();
    connection!.off('receiveMsgTemp');
    tableChatListProvider.close2();
  }

  /// 获取联系人的connID
  getContactsConnId() {
    User user;
    Http().post(Urls.GET_USER_INFO, {}, data: {
      "phone": contact!.phone,
    }, success: (json) {
      user = User.fromJson(json);
      print(json);
      contact = user;
      print(contact!.nickName);
      print(contact!.connID);
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {
      getUserConnId();
    });
  }

  /// 获取自己的connID
  getUserConnId() async {
    await getUserName();
    User user;
    Http().post(Urls.GET_USER_INFO, {}, data: {
      "phone": userName,
    }, success: (json) {
      user = User.fromJson(json);
      print(json);
      this.user = user;
      print(user.nickName);
      print(user.connID);
    }, fail: (reason, statusCode) {
      Fluttertoast.showToast(
          msg: reason.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0);
      print(reason);
    }, after: () {
      connection!.on('receiveMsgTemp', (message) {
        Map<String, dynamic> map = jsonDecode(jsonEncode(message.first));
        SendMessageTemplateModel sendMessageTemplateModel =
            SendMessageTemplateModel(message: map["message"]);

        ///todo message.first.toString()
        messageList!.add(ChatRecord(
            content: sendMessageTemplateModel.message,
            avatarUrl: 'img/atnw.jpg',
            sender: 0));

        ifNewMsg = false;
        ChatLogs chatLogs = ChatLogs(logs: messageList);
        String chatLogsToJson = json.encode(chatLogs);
        addChatListToSq(chatLogsToJson, contact!.phone);
//        updateChatListLogs(chatLogsToJson, contact!.phone);
        notifyListeners();
      });
    });
  }

  /// 发送消息
  sendMessage(BuildContext context) {
    if (isWord) {
      messageList!.add(ChatRecord(
          avatarUrl: 'img/head_portrait.png',
          sender: 1,
          content: sendController.text));
      moveToButton();

      // 发送至服务器
      sendMessageToServer(sendController.text);
      // 接收服务器传来的消息

      sendController.clear();
      notifyListeners();
    }

    ChatLogs chatLogs = ChatLogs(logs: messageList);
    String chatLogsToJson = json.encode(chatLogs);
    addChatListToSq(chatLogsToJson, contact!.phone);
//    updateChatListLogs(chatLogsToJson, contact!.phone);
  }

  /// 发送消息给服务器
  sendMessageToServer(msg) {
    // invoke 调用服务器中的哪个方法
    connection!.invoke('receiveMsg', args: [
      jsonEncode(SendMessageTemplateModel(
              fromWho: user!,
              toWho: contact!,
              message: msg,
              sendTime: DateTime.now().toString())
          .toJson())
    ]);
    connection!.invoke('receiveMsgTemp', args: [
      jsonEncode(SendMessageTemplateModel(
              fromWho: user!,
              toWho: contact!,
              message: msg,
              sendTime: DateTime.now().toString())
          .toJson())
    ]);
    notifyListeners();
  }

  /// 发送消息按钮颜色判断
  isEmpty(BuildContext context) {
    if (sendController.text.isNotEmpty) {
      sendButtonColor =
          Theme.of(context).bottomNavigationBarTheme.selectedItemColor!;
      isWord = true;
    }
    if (sendController.text.isEmpty) {
      sendButtonColor = const Color(0xffd1f3ff);
      isWord = false;
    }
    notifyListeners();
  }

  /// 移动到底部
  moveToButton() {
    ifNewMsg = true;
    msgListController.animateTo(
      messageList!.length * 66.0,
      duration: const Duration(milliseconds: 600),
      curve: Curves.ease,
    );
    notifyListeners();
  }

  /// 将聊天记录储存到数据库中
  addChatListToSq(String chat, String? contactPhone) async {
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, "chatList.db");
//    await deleteDatabase(path);
//    print('先删除数据量');

    await tableChatListProvider.open2(path);

    ChatList dd =
        await tableChatListProvider.getDataUseContactPhone2(contactPhone!);
    print('查询结果: ${dd.toMap()}');

    if (dd.mContactPhone == null) {
      // 插入新的数据
      ChatList chatList = ChatList();
      chatList.mChatList = chat;
      chatList.mContactPhone = contactPhone;
      ChatList td = await tableChatListProvider.insert(chatList);
      print('插入:${td.toMap()}');
    } else {
      try {
        updateChatListLogs(chat, contactPhone);
        ifSaveLogs = true;
//      Fluttertoast.showToast(
//          msg: '保存成功',
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.BOTTOM,
//          fontSize: 16.0);
      } catch (e) {
        print('错误');
      }
    }
    ChatList d =
        await tableChatListProvider.getDataUseContactPhone2(contactPhone);

    print('查询结果: ${d.toMap()}');
    await tableChatListProvider.close2();
  }

  /// 更新数据库
  updateChatListLogs(String chat, String? contactPhone) async {
    print("更新数据库");
    ChatList chatList = ChatList();
    chatList.mChatList = chat;
    chatList.mContactPhone = contactPhone;
    await tableChatListProvider.update(chatList, contactPhone!);
    ChatList dd =
        await tableChatListProvider.getDataUseContactPhone2(contactPhone);

    print('查询结果: ${dd.toMap()}');
  }

  /// 从本地数据库中获得聊天记录
  getChatLogsFromDb(String? contactPhone) async {
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, "chatList.db");

//    await deleteDatabase(path);
    await tableChatListProvider.open2(path);
    //查询数据，获取用户名和密码
    ChatList dd =
        await tableChatListProvider.getDataUseContactPhone2(contactPhone!);
    if (dd.mChatList != null) {
      print(dd.mChatList!);
      Map<String, dynamic> map = jsonDecode(dd.mChatList!);
      print(map);
      List tempLogs = map["logs"];
      print("tempLogs" + tempLogs.toString());
      for (int i = 0; i < tempLogs.length; i++) {
        ChatRecord mChatRecord = ChatRecord.fromJson(tempLogs[i]);
        record!.add(mChatRecord);
        print('记录信息: ' + mChatRecord.content.toString());
      }
      record!.add(ChatRecord(
          content: message!.last.message,
          avatarUrl: 'img/atnw.jpg',
          sender: 0));
      messageList = record;
      notifyListeners();
    }
    await tableChatListProvider.close2();
  }
}
